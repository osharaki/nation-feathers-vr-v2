﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using VRStandardAssets.Utils;

/// <summary>
/// Controls behaviour of interactive items in Challenge phase
/// </summary>
public class ChallengeVRInteractiveItemApplied : MonoBehaviour {

    public VRInteractiveItem interactiveItem;

    private GameObject thisDustContainer;
    private SignpostDrop signpost;

    private Image infoboxImage,
                  infoboxImageGeneric;
    private Text infoboxTextbox;
    private GameObject infoboxVideoQuad;
    private Reticle r;

    private void Start()
    {
        r = Camera.main.GetComponent<Reticle>();
        //Getting references to local infobox's components
        foreach (Image i in gameObject.GetComponentsInChildren<Image>(true))
        {
            if (i.gameObject.tag == "Infobox_Image")
                infoboxImage = i;
            if (i.gameObject.tag == "Infobox_Image_Generic")
                infoboxImageGeneric = i;
        }
        infoboxTextbox = gameObject.GetComponentInChildren<Text>(true);
        infoboxVideoQuad = gameObject.GetComponentInChildren<VideoPlayer>(true).gameObject.transform.parent.gameObject;

        signpost = GetComponentInChildren<SignpostDrop>(true);
        interactiveItem = GetComponent<VRInteractiveItem>();
        if (interactiveItem != null)
        {
            interactiveItem.OnOver += HandleOver;
            interactiveItem.OnOut += HandleOut;
            interactiveItem.OnClick += HandleClick;
        }

        //thisDustContainer = IntroInterfaceController.instance.dustContainer;
        /*if(thisDustContainer != null)
            thisDustContainer = GetComponentInChildren<ParticleSystem>(true).transform.parent.gameObject;*/
    }

    private void OnEnable()
    {
        signpost = GetComponentInChildren<SignpostDrop>(true);
        interactiveItem = GetComponent<VRInteractiveItem>();
        if (interactiveItem != null)
        {
            interactiveItem.OnOver += HandleOver;
            interactiveItem.OnOut += HandleOut;
            interactiveItem.OnClick += HandleClick;
        }
    }

    private void OnDisable()
    {
        if (interactiveItem != null)
        {
            interactiveItem.OnOver -= HandleOver;
            interactiveItem.OnOut -= HandleOut;
            interactiveItem.OnClick -= HandleClick;
        }
    }

    private void HandleOver()
    {
        //Debug.Log("Object is highlighted");
        if (thisDustContainer != null)
        {
            //thisDustContainer.transform.GetChild(0).gameObject.SetActive(true);
            
            
        }
        if (r != null)
        {
            r.Show(); //Activate large reticle
        }
    }

    private void HandleOut()
    {
        //Debug.Log("Object is NOT highlighted");
        if (thisDustContainer != null)
        {
            //thisDustContainer.transform.GetChild(0).gameObject.SetActive(false);
            
            
        }
        if (r != null)
        {
            r.Hide(); //Deactivate large reticle
        }
    }

    public void HandleClick()
    {
        AnswerManager answerManager = GetComponent<AnswerManager>();
        if(answerManager != null)
        {
            if (!answerManager.answered) //if not yet answered
            {                            
                if (ChallengeAudioController.instance.isFocused)
                {
                    if (ChallengeAudioController.instance.focusedObject == gameObject)
                    {
                        ChallengeAudioController.instance.focusedObject = gameObject;
                        ChallengeAudioController.instance.isFocused = !ChallengeAudioController.instance.isFocused;
                        //InterfaceController.instance.ManageInfobox();
                        if (signpost.gameObject != null)
                            signpost.DeactivateSignpostComponents();
                        InterfaceController.instance.DeactivateInfoboxImage(infoboxImage.gameObject);
                        InterfaceController.instance.DeactivateInfoboxText(infoboxTextbox.gameObject);
                        InterfaceController.instance.DeactivateInfoboxVideo(infoboxVideoQuad);
                    }
                }
                else
                {
                    ChallengeAudioController.instance.focusedObject = gameObject;
                    ChallengeAudioController.instance.isFocused = !ChallengeAudioController.instance.isFocused;
                    //InterfaceController.instance.ManageInfobox();
                    if (signpost.gameObject != null)
                    {
                        signpost.ActivateSignpostComponents();
                        signpost.FallStarter();
                    }
                    InterfaceController.instance.ActivateInfoboxGenericImage(infoboxImageGeneric.gameObject);
                    

                    if (gameObject == ChallengeAudioController.instance.targetObject &&
                        ChallengeAudioController.instance.audioSourceList.Count == 0)
                    {
                        if (GameController.currentDifficulty < GameController.maxDifficulty)
                        {
                            GameController.currentDifficulty++;
                            ChallengeAudioController.instance.alreadyGotAudioList = false;
                        }
                        else if (!ChallengeAudioController.instance.waveOver)
                        {
                            ChallengeAudioController.instance.waveOver = !ChallengeAudioController.instance.waveOver;
                        }
                    }
                }
            }
            else //if answered
            {
                if (ChallengeAudioController.instance.isFocused)
                {
                    if (ChallengeAudioController.instance.focusedObject == gameObject)
                    {
                        ChallengeAudioController.instance.focusedObject = gameObject;
                        ChallengeAudioController.instance.isFocused = !ChallengeAudioController.instance.isFocused;
                        InterfaceController.instance.ManageInfoboxChallenge();

                        //InterfaceController.instance.AddDustEffect();
                        //Debug.Log("Object has been clicked");
                    }
                }
                else
                {
                    ChallengeAudioController.instance.focusedObject = gameObject;
                    ChallengeAudioController.instance.isFocused = !ChallengeAudioController.instance.isFocused;
                    InterfaceController.instance.ManageInfoboxChallenge();
                    if (gameObject == ChallengeAudioController.instance.targetObject &&
                        ChallengeAudioController.instance.audioSourceList.Count == 0)
                    {
                        if (GameController.currentDifficulty < GameController.maxDifficulty)
                        {
                            GameController.currentDifficulty++;
                            ChallengeAudioController.instance.alreadyGotAudioList = false;
                        }
                        else if (!ChallengeAudioController.instance.waveOver)
                        {
                            ChallengeAudioController.instance.waveOver = !ChallengeAudioController.instance.waveOver;
                        }
                    }
                    //InterfaceController.instance.AddDustEffect();
                    //Debug.Log("Object has been clicked");
                }
            }
        }        
    }
}
