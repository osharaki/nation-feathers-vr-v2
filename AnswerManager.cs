﻿//using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

/// <summary>
/// Manages the question and answers posed at each tree. 
/// </summary>
public class AnswerManager : MonoBehaviour {

    public bool answered; /*!< Whether the question has been answered correctly for this tree.*/
    public string correctAnswer;
    public GameObject localInfobox;

    private GameObject[] options;
    private Image image, image_generic;    
    private GameObject videoQuad;
    private Text infoboxText;

    // Use this for initialization
    void Start()
    {
        options = new GameObject[4];
        int chosenSignIndex, chosenFileIndex;        
        Sprite chosenImage = null;
        //List<AudioClip> audioFiles = DBController.instance.AudioFiles;
        List<Sprite> imageFiles = DBController.instance.ImageFiles;
        chosenFileIndex = Random.Range(0, GameController.instance.sizeOfPools);

        foreach (Transform t in transform)
        {
            if (t.gameObject.tag == "Infobox")
            {
                localInfobox = t.gameObject;
                break;
            }
        }
        answered = false;
        Text birdName = localInfobox.GetComponentInChildren<Text>(true);
        foreach(Image i in localInfobox.GetComponentsInChildren<Image>(true))
        {
            if(i.tag == "Infobox_Image")
            {
                chosenImage = i.sprite;
                break;
            }
        }

        if (birdName != null)
        {
            correctAnswer = birdName.text;
        }
        List<List<string>> namesANDfiles = DBController.instance.GetBirdNames(correctAnswer);
        for (int i = 0; i < namesANDfiles.Count; i++)
        {
            List<string> temp = namesANDfiles[i];
            int randomIndex = Random.Range(i, namesANDfiles.Count);
            namesANDfiles[i] = namesANDfiles[randomIndex];
            namesANDfiles[randomIndex] = temp;
        }
        
        foreach(Transform t in transform)
        {
            if(t.gameObject.tag == "Signpost")
            {
                foreach(Transform t2 in t)
                {
                    if(t2.gameObject.tag == "Signs")
                    {
                        chosenSignIndex = Random.Range(0, 4);
                        for (int i = 0; i < 4; i++)
                        {
                            Image image = t2.GetChild(i).GetComponentInChildren<Image>(true);
                            TextMeshProUGUI text = t2.GetChild(i).GetComponentInChildren<TextMeshProUGUI>(true);
                            string[] splitLatin, splitSpace;
                            string finalName;
                            if (i == chosenSignIndex)
                            {
                                t2.GetChild(i).GetComponent<VRInteractiveSignpost>().isCorrectAnswer = true;
                                if(chosenImage != null)
                                {
                                    image.sprite = chosenImage;
                                }
                                //string manipulation..
                                splitLatin = birdName.text.Split('\n');
                                splitSpace = splitLatin[0].Split();
                                finalName = splitSpace[0]
                                            + '\n'
                                            + splitSpace[1];
                                text.text = finalName;
                                AspectRatio(image, 2);
                                continue;
                            }                            
                            image.sprite = DBController.instance.GetImageFromName(namesANDfiles[i][1]);
                            //string manipulation..
                            splitLatin = namesANDfiles[i][0].Split('\n');
                            splitSpace = splitLatin[0].Split();
                            finalName = splitSpace[0]
                                        + '\n'
                                        + splitSpace[1];
                            text.text = finalName;
                            AspectRatio(image, 2);
                        }
                        break;
                    }                    
                }
            }
        }
    }

    //I want this to be called when a sign is clicked on the signpost.
    /// <summary>
    /// Checks if answer chosen by player is the correct answer.
    /// </summary>
    /// <param name="answerGiven"></param>
    /// <returns>True if answer is correct.</returns>
    public bool CheckAnswerGiven(string answerGiven)
    {
        if(answerGiven == correctAnswer)
        {
            answered = true;
            return true;
        }
        return false;
    }

    /// <summary>
    /// From a list of integers chooses a random number.
    /// Never returns the same number again.
    /// </summary>
    /// <param name="aList"></param>
    /// <returns>Unique random number from an integer list.</returns>
    int GetRandom(List<int> aList)
    {
        if (aList.Count == 0)
        {
            return -1;
        }
        int randInd = Random.Range(0, aList.Count);
        int result = aList[randInd];
        aList.RemoveAt(randInd);
        return result;
    }

    /// <summary>
    /// Changes image width accordingly given a custom height 
    /// to maintain aspect ratio of original image. 
    /// </summary>
    /// <param name="raw">Image to be manipulated.</param>
    void AspectRatio(Image raw, float customHeight)
    {
        //Note: x component of raw.rectTransform.sizeDelta is width
        raw.SetNativeSize();
        float height;
        float width;
        height = raw.rectTransform.sizeDelta.y;
        width = raw.rectTransform.sizeDelta.x;
        //Debug.Log(raw.rectTransform.sizeDelta);

        float ratio = width / height;

        //float newWidth = ratio * 100;

        raw.rectTransform.sizeDelta = new Vector2(ratio * customHeight, customHeight);
    }

    /// <summary>
    /// Disables generic image in local infobox while enabling all other 
    /// components.
    /// </summary>
    public void ActivateInfobox(SpriteRenderer sr)
    {
        if (sr != null)
            sr.gameObject.SetActive(false);
        //Get all components
        infoboxText = localInfobox.GetComponentInChildren<Text>(true);
        foreach (Image i in localInfobox.GetComponentsInChildren<Image>(true))
        {
            if (i.gameObject.tag == "Infobox_Image")
                image = i;
            if (i.gameObject.tag == "Infobox_Image_Generic")
                image_generic = i;
        }
        videoQuad = localInfobox.GetComponentInChildren<VideoPlayer>(true).transform.parent.gameObject;

        //Activate and deactivate components
        videoQuad.SetActive(true);
        infoboxText.gameObject.SetActive(true);
        image.gameObject.SetActive(true);
        image_generic.gameObject.SetActive(false);
    }
}
