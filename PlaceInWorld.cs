﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

/// <summary>
/// UI objects that this script is attached to are placed in world space.
/// </summary>
public class PlaceInWorld : MonoBehaviour {

    public Camera myCamera;
    public float distFromCamera,
                 heightOffGround;
    public bool useNormal,
                movePosY, /*!< Determines whether to place the object at a certain height.*/
                isLive;   /*!< Determines whether the object should move as the player looks around.*/

                          // Use this for initialization
    void Start () {
        if (!movePosY)
            transform.position = new Vector3(myCamera.GetComponent<VREyeRaycaster>().endOfRay.x + distFromCamera,
                                         heightOffGround,
                                         myCamera.GetComponent<VREyeRaycaster>().endOfRay.z - distFromCamera);
        else
            transform.position = new Vector3(myCamera.GetComponent<VREyeRaycaster>().endOfRay.x,
                                         myCamera.GetComponent<VREyeRaycaster>().endOfRay.y,
                                         myCamera.GetComponent<VREyeRaycaster>().endOfRay.z);

    }
	
	// Update is called once per frame
	void Update () {
        if (isLive && movePosY)
        {
            transform.position = new Vector3(myCamera.GetComponent<VREyeRaycaster>().endOfRay.x,
                                         Mathf.Clamp(myCamera.GetComponent<VREyeRaycaster>().endOfRay.y, 2, 6),
                                         myCamera.GetComponent<VREyeRaycaster>().endOfRay.z);
        }
        else if (isLive && !movePosY)
        {
            transform.position = new Vector3(myCamera.GetComponent<VREyeRaycaster>().endOfRay.x,
                                         heightOffGround,
                                         myCamera.GetComponent<VREyeRaycaster>().endOfRay.z);
        }
    }
}
