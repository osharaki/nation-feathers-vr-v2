﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using VRStandardAssets.Utils;

/// <summary>
/// Organizes and manages game flow and system variables.
/// </summary>
public class GameController : MonoBehaviour {

    public enum PlayMode
    {
        INTRO,
        LEARN,
        CHALLENGE
    };

    /// <summary>
    /// The time of year. Used to select birds to be generated.
    /// </summary>
    public enum Season
    {
        WINTER,
        SPRING,
        SUMMER,
        AUTUMN
    };        

    /// <summary>
    /// The time of day. Used to select birds to be generated.
    /// </summary>
    public enum TimeOfDay
    {
        DAWN,
        NOON,
        DUSK,
        NIGHT
    };

    /// <summary>
    /// Where the birds generally vocalize from. Used to select birds to be generated.
    /// </summary>
    public enum Habitat
    {
        BASIC,
        WATER
    }

    public enum SceneName
    {
        INTRO,
        DAY,
        NIGHT,
        MENU
    }

    /// <summary>
    /// Contains information relevant to the intro scene about where exactly
    /// in the narrative the player is right now.
    /// </summary>
    public enum NarrativeState
    {
        INITIAL,    //Start of intro.
        CENTERED,   //Player has centered controlller.
        MOVED,      //Player has reached movement checkpoint.
        LANDED,     //Bird has finished circling and has landed on a tree.
        HIGHLIGHTED,//Player's gaze has fallen on the tree with the bird.
        FOCUSED,    //Player has clicked the tree.
        VIDEO,      //Spectrogram has been played
        UNFOCUSED   //Player has clicked the tree once more unfocusing it. 
    }

    public enum MenuSelection
    {
        INTRO,
        REPLAY_DAY,
        REPLAY_CHALLENGE
    }

    public static GameController instance;
    public MenuSelection menuSelection; /*!< The selected option in the menu scene.*/     
    public static int currentDifficulty; /*!< current difficulty level; 1 being the easiest*/
    public static int maxDifficulty; /*!< The maximum sound difficulty to be retrieved from the DB.*/
    public static Season currentSeason { get; private set; } /*!< Current season depending on scene.*/
    public static Habitat currentTerrain { get; private set; } /*!< Current habitat depending on scene.*/
    public static TimeOfDay currentTOD { get; private set; } /*!< Current time of day depending on scene.*/
    public NarrativeState narrative; /*!< Used by the intro scene. Tells where the player is in the intro narrative.*/
    public int sizeOfPools;
    //public bool learnOver; /*!< Is true when player has found all birds in learn phase.*/
    public GameObject circlingBird,
                      soundIntroContainer,
                      checkpointContainer;
                      //gatewayEffects;
    public Camera myCamera;
    public bool checkDistance; /*!< Used to determine whether trees will be unfocused if player moves away from them.*/
    public string sceneName;
    public SceneName sceneEnum;

    private GameObject infobox,
                       player;

    void Awake()
    {
        instance = this;
        myCamera = Camera.main;        
    }

    // Use this for initialization
    void Start () {

        myCamera.GetComponent<VRCameraFade>().OnFadeOutComplete += LoadNextScene;
        player = GameObject.FindWithTag("Player");
        maxDifficulty = 2;
        checkDistance = true;
        //SceneManager.sceneLoaded += OnNewSceneLoad;

        //myCamera.GetComponent<VRCameraFade>().OnFadeOutComplete += LoadMenu;

        //OVRInput.RecenterController(); //Recentering in an attempt to activate controller without having to click randomly

        currentDifficulty = 1;
        currentTOD = TimeOfDay.NOON;
        sceneName = SceneManager.GetActiveScene().name;

        if (sceneName == "Intro")
        {
            sceneEnum = SceneName.INTRO;
            narrative = NarrativeState.CENTERED;
            //circlingBird = GameObject.FindGameObjectWithTag("Circling Bird");
            soundIntroContainer = GameObject.FindGameObjectWithTag("Sound Intro Container");
            checkpointContainer = GameObject.FindGameObjectWithTag("Checkpoint Container");
            //gatewayEffects = GameObject.FindGameObjectWithTag("Gateway Effects").GetComponentInChildren<AudioSource>(true).transform.parent.gameObject;
            StartCoroutine("Introduction");
        }
        else if (sceneName == "Basic Forest Summer")
        {
            //generate summer bird sounds (no water birds)
            //difficulty must match the currentDifficulty variable
            sceneEnum = SceneName.DAY;
            currentSeason = Season.SUMMER;
            currentTerrain = Habitat.BASIC;
            //gatewayEffects = GameObject.FindGameObjectWithTag("Gateway Effects").GetComponentInChildren<AudioSource>(true).transform.parent.gameObject;
            StartCoroutine("DayScene");
        }
        else if (sceneName == "Challenge Forest Summer")
        {
            sceneEnum = SceneName.DAY;
            currentSeason = Season.SUMMER;
            currentTerrain = Habitat.BASIC;
            //gatewayEffects = GameObject.FindGameObjectWithTag("Gateway Effects").GetComponentInChildren<AudioSource>(true).transform.parent.gameObject;
            StartCoroutine("DaySceneChallenge");
        }
        else if (sceneName == "Menu")
        {
            //This will be carried out if we're in the main menu.
            //In that case we want to set everything so that it's as if 
            //we're in the Basic Forest Summer scene**
            StopAllCoroutines();
            sceneEnum = SceneName.MENU;
            currentSeason = Season.SUMMER;
            currentTerrain = Habitat.BASIC;
        }

        //DontDestroyOnLoad(this);
    }

    /// <summary>
    /// Gets called every time a new scene is loaded and sets the GameController's parameters 
    /// in accordance with the new scene.
    /// </summary>
    /// <param name="s">Scene that has been loaded.</param>
    /// <param name="lsm"></param>
    /*void OnNewSceneLoad(Scene s, LoadSceneMode lsm)
    {
        //Debug.Log("Scene name: " + s.name);
        myCamera = Camera.main;
        maxDifficulty = 2;
        checkDistance = true;
        SceneManager.sceneLoaded += OnNewSceneLoad;

        myCamera.GetComponent<VRCameraFade>().OnFadeOutComplete += LoadNextScene;
        sceneName = s.name;
        if (sceneName == "Intro")
        {
            sceneEnum = SceneName.INTRO;
            narrative = NarrativeState.CENTERED;
            //circlingBird = GameObject.FindGameObjectWithTag("Circling Bird");
            soundIntroContainer = GameObject.FindGameObjectWithTag("Sound Intro Container").transform.GetChild(0).gameObject;
            checkpointContainer = GameObject.FindGameObjectWithTag("Checkpoint Container");
            gatewayEffects = GameObject.FindGameObjectWithTag("Gateway Effects").GetComponentInChildren<AudioSource>(true).transform.parent.gameObject;
            GameObject[] aList = GameObject.FindGameObjectsWithTag("GameController");
            if (aList.Length > 1)
            {
                Destroy(aList[0].gameObject); //If a GameController object is not destroyed from the intro scene, there would be 2 of these objects in the intro scene. 
            }
            StartCoroutine("Introduction");
        }
        else if (sceneName == "Basic Forest Summer")
        {
            //generate summer bird sounds (no water birds)
            //difficulty must match the currentDifficulty variable
            sceneEnum = SceneName.DAY;
            currentSeason = Season.SUMMER;
            currentTerrain = Habitat.BASIC;
            gatewayEffects = GameObject.FindGameObjectWithTag("Gateway Effects").GetComponentInChildren<AudioSource>(true).transform.parent.gameObject;
            StartCoroutine("DayScene");
        }
        else if(sceneName == "Menu")
        {
            //This will be carried out if we're in the main menu.
            //In that case we want to set everything so that it's as if 
            //we're in the Basic Forest Summer scene
            StopAllCoroutines();
            sceneEnum = SceneName.MENU;
            currentSeason = Season.SUMMER;
            currentTerrain = Habitat.BASIC;
        }
    }*/

    /// <summary>
    /// Regulates the intro scene's narrative/sequence of events.
    /// </summary>
    /// <returns></returns>
    IEnumerator Introduction()
    {
        GameObject tree = null;
        if(narrative == NarrativeState.INITIAL)
        {
            //Display message to center controller...
            //Debug.Log("Initial state. Now, to center.");
            while (narrative == NarrativeState.INITIAL) //while still hasn't centered, wait..
            {
                //check if player has centered and if yes change narrative..
                yield return null;
            }
        }
        if(narrative == NarrativeState.CENTERED) //If player has centered controller..
        {
            //Add checkpoint point and prompt player to move towards it..
            //Debug.Log("Player has centered. Now, to move.");
            checkpointContainer.SetActive(true);
            while (narrative == NarrativeState.CENTERED) //while player still hasn't reached CP, wait..
            {
                //Check if player has reached checkpoint
                yield return null;
            }
        }
        if(narrative == NarrativeState.MOVED) //If player has reached checkpoint..
        {
            //Initiate flying bird sequence..
            //Debug.Log("Player has moved. Now, to initiate flight sequence.");
            //IntroInterfaceController.instance.ActivateHearPrompt();
            player.GetComponent<PlayerController>().isGrounded = true;
            soundIntroContainer.transform.GetChild(0).gameObject.SetActive(true);
            circlingBird = GameObject.FindGameObjectWithTag("Circling Bird");
            MovingBird mb = circlingBird.GetComponent<MovingBird>();
            while (narrative == NarrativeState.MOVED) //while sequence still ongoing, wait..
            {
                //Check if sequence is over                                
                tree = mb.CircleThenLand();
                yield return null;
            }
            player.GetComponent<PlayerController>().isGrounded = false;
            //disable tree interaction till prompt has been shown
            if (tree != null)
            {
                IntroVRInteractiveItemApplied interactionScript = 
                    tree.GetComponent<IntroVRInteractiveItemApplied>();
                if(interactionScript != null)
                {
                    interactionScript.enabled = false;
                }
            }
        }
        if(narrative == NarrativeState.LANDED) //If sequence is over and bird has landed..
        {
            //Prompt player to gaze at tree
            //Debug.Log("Flight sequence complete. Now, to gaze.");
            IntroInterfaceController.instance.EnableGazeHint();
            
            //Enable tree interaction again
            IntroVRInteractiveItemApplied interactionScript =
                    tree.GetComponent<IntroVRInteractiveItemApplied>();
            if (interactionScript != null)
            {
                interactionScript.enabled = true;
            }

            while (narrative == NarrativeState.LANDED) //while player still hasn't gazed, wait..
            {
                //Check if player has gazed..
                IntroInterfaceController.instance.ManageGazeHint();
                yield return null;
            }
            IntroInterfaceController.instance.DisableGazeHint();            
        }
        if(narrative == NarrativeState.HIGHLIGHTED) //If player gazed at tree..
        {
            //Prompt player to click tree
            //Debug.Log("Gaze complete. Now, to click.");
            IntroInterfaceController.instance.ActivateClickPrompt();
            while (narrative == NarrativeState.HIGHLIGHTED) //while not clicked, wait..
            {
                //Check if player has clicked..
                yield return null;
            }

            //Disable tree interaction to prevent player from unfocusing tree.
            IntroVRInteractiveItemApplied interactionScript =
                    tree.GetComponent<IntroVRInteractiveItemApplied>();
            if (interactionScript != null)
            {
                interactionScript.enabled = false;
            }
            Reticle r = myCamera.GetComponent<Reticle>();
            r.Hide();
            checkDistance = false;
        }
        if(narrative == NarrativeState.FOCUSED) //If player has clicked tree..
        {
            //Wait for some seconds to give the player a chance to see infobox
            //then activate look-right-prompt.
            //Find video in tree. Add prompt beside video symbol. 
            //Debug.Log("Click complete. Now, to watch video.");
            yield return new WaitForSeconds(2);
            if (tree != null)
            {
                GameObject videoQuad = 
                tree.GetComponentInChildren<VideoPlayer>(true).transform.parent.gameObject;
                VideoInteractive vdInter = null;
                if (videoQuad != null)
                    vdInter = videoQuad.GetComponent<VideoInteractive>();
                if(vdInter != null)
                {
                    //Enable prompt
                    IntroInterfaceController.instance.ActivateLookRightPrompt(tree);

                    while (!vdInter.videoPlayer.isPlaying)
                    {
                        //wait while video hasn't been played yet
                        yield return null;
                    }
                    //Debug.Log("Now playing");
                    //Once video has been played DISBALE PROMPT & 
                    //wait for it to finish.
                    IntroInterfaceController.instance.DeactivateLookRightPrompt();

                    while (vdInter.videoPlayer.isPlaying && (ulong)vdInter.videoPlayer.frame != vdInter.videoPlayer.frameCount)
                    {
                        yield return null;
                    }
                    //Debug.Log("Video done");
                    narrative = NarrativeState.VIDEO;
                }
                //Enable tree interaction again.
                IntroVRInteractiveItemApplied interactionScript =
                        tree.GetComponent<IntroVRInteractiveItemApplied>();
                if (interactionScript != null)
                {
                    interactionScript.enabled = true;
                }
                checkDistance = true;
            }                        
        }
        if (narrative == NarrativeState.VIDEO)
        {
            //Prompt player to unclick tree
            //Debug.Log("Video complete. Now, to unclick.");
            IntroInterfaceController.instance.ActivateUnclickPrompt();            
            while (narrative == NarrativeState.VIDEO) //while not unclicked, wait..
            {
                //Check if unclicked..
                yield return null;
            }
        }
        if(narrative == NarrativeState.UNFOCUSED)
        {
            //Add gateway with sound..(Coroutine will then terminate on it's own).
            yield return new WaitForSeconds(2);
            myCamera.GetComponent<VRCameraFade>().FadeOut(false);
            /*ActivateGatewayEffects();
            IntroInterfaceController.instance.ActivateGatewayArrow();*/
            //Debug.Log("Unclick complete. Add gate and exit coroutine.");
        }
    }

    /// <summary>
    /// Regulates day scene's narrative/sequence of events.
    /// </summary>
    /// <returns></returns>
    IEnumerator DayScene()
    {
        while (!AudioController.instance.waveOver)
        {
            yield return null;
        }
        yield return new WaitForSeconds(5);
        InterfaceController.instance.ActivatePressBackButton();
        //ActivateGatewayEffects();
    }

    /// <summary>
    /// Regulates Challenge day scene's narrative/sequence of events.
    /// </summary>
    /// <returns></returns>
    IEnumerator DaySceneChallenge()
    {
        while (!ChallengeAudioController.instance.waveOver)
        {
            yield return null;
        }
        yield return new WaitForSeconds(5);
        InterfaceController.instance.ActivatePressBackButton();
        //ActivateGatewayEffects();
    }

    /// <summary>
    /// Activates gateway effects and trigger to load next scene upon entering.
    /// </summary>
    void ActivateGatewayEffects()
    {
        //gatewayEffects.SetActive(true);
    }

    /// <summary>
    /// Loads next scene. Next scene is determined by current scene.
    /// Intro scene -> Day scene
    /// Day scene -> Menu scene
    /// 
    ///            -> Intro scene  
    /// Menu scene or
    ///            -> Day scene 
    /// </summary>
    /// <param name="sceneName"></param>
    public void LoadNextScene(string sceneName = null)
    {
        /*if(sceneName == null)
        {
            if (sceneEnum == SceneName.INTRO)
            {
                SceneManager.LoadScene("Basic Forest Summer");
            }
            if (sceneEnum == SceneName.DAY)
            {
                //what to do when player enters gate in day scene?
            }
        }
        else
        {
            if(sceneName == "Intro")
            {
                SceneManager.LoadScene("Basic Forest Summer");
            }
            else if(sceneName == "Basic Forest Summer")
            {
                SceneManager.LoadScene("Challenge Forest Summer");
            }
            else if (sceneName == "Challenge Forest Summer")
            {
                SceneManager.LoadScene("Menu");
            }
            else if(sceneName == "Menu")
            {
                if(menuSelection == MenuSelection.INTRO)
                    SceneManager.LoadScene("Intro");
                else if(menuSelection == MenuSelection.REPLAY_DAY)
                    SceneManager.LoadScene("Basic Forest Summer");
                else if (menuSelection == MenuSelection.REPLAY_CHALLENGE)
                    SceneManager.LoadScene("Challenge Forest Summer");
            }            
        }*/
        if (sceneName == "Intro")
        {
            LoadMenu();
        }
        else if (sceneName == "Basic Forest Summer")
        {
            LoadMenu();
        }
        else if (sceneName == "Challenge Forest Summer")
        {
            LoadMenu();
        }
        else if (sceneName == "Menu")
        {
            if (menuSelection == MenuSelection.INTRO)
                SceneManager.LoadScene("Intro");
            else if (menuSelection == MenuSelection.REPLAY_DAY)
                SceneManager.LoadScene("Basic Forest Summer");
            else if (menuSelection == MenuSelection.REPLAY_CHALLENGE)
                SceneManager.LoadScene("Challenge Forest Summer");
        }
    }

    public void LoadMenu(string sceneName = null)
    {
        SceneManager.LoadScene("Menu");
    }
}
