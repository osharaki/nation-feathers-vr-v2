﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;
using VRStandardAssets.Utils;

public class ChallengeAudioController : MonoBehaviour {

    public static ChallengeAudioController instance;
    public float maxDist;   /*!<The max distance that the player can hear the AudioSource.*/
    public GameObject player;
    public GameObject audioSourceTemplate; /*!< Reference to audio source template to be replicated when creating all audio sources.*/
    public bool isFocused = false; /*!< Shows whether a tree is currently being inspected by the player.*/
    public GameObject focusedObject; /*!< Reference to the tree being inspected.*/
    public AudioSource targetSource; /*!< Reference to target object's audio source component.*/
    public GameObject targetObject; /*!< Reference to the object, e.g. tree, containing the sound object that the player needs to listen for next.*/
    public bool waveOver = false;  /*!< Signals when player has finished all birds in level/wave.*/
    public List<AudioClip> audioSourceList; /*!< Used to save audio clips retrieved from the DB.*/
    public int Index; /*!< Index of chosen sound clip.*/
    public bool alreadyGotAudioList = false; /*!< Set to true if a list of viable audio clips has been retrieved from DB and saved in audioSourceList.*/

    private List<GameObject> trees; /*!< Trees to choose from to add interactability to.*/
    private bool addNewSound;   /*!<Is set to true when the player has found and interacted with the previous AudioSource.*/
    //[SerializeField] private int soundObjectsInPool;
    private GameObject[] soundObjectPool; /*!< Object pool that contains a specific number of sound objects. These are the sound objects that are eventually distributed to the trees.*/
    public List<GameObject> activeSoundObjects; /*!< Sound objects that have been selected from pool to be added to trees.*/
    private GameObject targetSoundObject; /*!< The sound object that players should currently be looking for.*/
    private GameObject forest; /*!< Reference to game object which contains all the trees.*/


    void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start()
    {
        //templateAudioSource = audioSourceTemplate.GetComponent<AudioSource>();
        audioSourceList = new List<AudioClip>();
        activeSoundObjects = new List<GameObject>();
        soundObjectPool = new GameObject[GameController.instance.sizeOfPools];
        for (int i = 0; i < soundObjectPool.Length; i++)
        {
            soundObjectPool[i] = Instantiate(audioSourceTemplate);
            soundObjectPool[i].SetActive(false);
        }

        trees = new List<GameObject>();
        forest = GameObject.FindGameObjectWithTag("Trees");
        if (forest != null)
        {
            foreach (Transform treeTransform in forest.transform)
            {
                if (treeTransform.gameObject.tag != "Other")
                    trees.Add(treeTransform.gameObject);
            }
        }
        else
        {
            Debug.Log("GameObject forest could not be found");
        }

        addNewSound = true;

    }

    // Update is called once per frame
    void Update()
    {
        AddInteractability();
        ControlSoundPlay();
    }

    /// <summary>
    /// Receives a list of AudioClips from the DB, choosing an AudioClip randomly and 
    /// adds it to the chosen tree. A custom AudioSource is used to hold the clip.
    /// A VRInteractiveItem, signpost and dustContainer are also added to the tree.
    /// </summary>
    void AddInteractability()
    {
        if (addNewSound && (!alreadyGotAudioList || audioSourceList.Count > 0))
        {
            //Choose a tree which is within hearing distance of the player
            foreach (GameObject tree in trees)
            {
                bool foundCompatibleUnusedTree = true;
                float dist = Vector3.Distance(tree.transform.position, player.transform.position);
                if (dist < 40 && dist > 30) //If the tree is within 10 units of hearing limit and
                {                               //doesn't already contain a sound child use it.
                    foreach (AudioSource childOfTree in
                        tree.GetComponentsInChildren<AudioSource>()) //If no child has AudioSource
                    {                                                //then tree is good.   
                        foundCompatibleUnusedTree = false;
                    }
                    if (foundCompatibleUnusedTree)
                    {
                        targetObject = tree;
                        break;
                    }
                }
            }

            //Get sound object from pool, activate it and place it 
            //where the tree is .
            targetSoundObject = GetSoundObject();
            if (targetSoundObject != null && targetObject != null)
            {
                targetSoundObject.SetActive(true);
                activeSoundObjects.Add(targetSoundObject);
                targetSource = targetSoundObject.GetComponent<AudioSource>();
                targetSoundObject.transform.parent = targetObject.transform;
                targetSoundObject.transform.localPosition = Vector3.zero;
                ChallengeVRInteractiveItemApplied VRinteractiveComponent =
                                            targetObject.AddComponent<ChallengeVRInteractiveItemApplied>();
                VRInteractiveItem VRinteractive = targetObject.AddComponent<VRInteractiveItem>();
                

                VRinteractiveComponent.interactiveItem = VRinteractive;
            }

            //Get an AudioSource list from DB and choose one randomly
            if (!alreadyGotAudioList)
            {
                audioSourceList = DBController.instance.GetASound();
                alreadyGotAudioList = !alreadyGotAudioList;
            }

            int len = audioSourceList.Count;
            int randInd = Random.Range(0, len);
            //Find index of chosen AudioClip in DBController's AudioFiles list.
            for (int i = 0; i < DBController.instance.AudioFiles.Count; i++)
            {
                if (DBController.instance.AudioFiles[i] == audioSourceList[randInd])
                {
                    Index = i;
                    break;
                }
            }

            if (targetSource != null)
                targetSource.clip = audioSourceList[randInd];
            audioSourceList.RemoveAt(randInd);
            addNewSound = !addNewSound;

            //Adding a dustContainer
            //InterfaceController.instance.AddDustEffect(targetObject);

            //Adding an infobox
            InterfaceController.instance.AddInfoboxChallenge(targetObject);

            //Adding a signpost
            InterfaceController.instance.AddSignpost(targetObject);
            targetObject.AddComponent<AnswerManager>();
        }
    }

    /// <summary>
    /// Controls which trees will be emitting sound at any given time.
    /// </summary>
    private void ControlSoundPlay()
    {
        if (!waveOver)
        {
            if (isFocused) //If object has been focused, make sure that only this object's sound is playing
            {
                if (focusedObject == targetObject)
                {
                    addNewSound = true;
                }
                else if (focusedObject != null)
                {
                    if (targetSource != null)
                    {
                        if (targetSource.isPlaying)
                        {
                            targetSource.Stop();
                        }
                    }

                    /*AudioSource focusedSource =
                        focusedObject.transform.GetChild(0).gameObject.GetComponent<AudioSource>();*/
                    AudioSource focusedSource = new AudioSource();
                    foreach (Transform t in focusedObject.transform)
                    {
                        if (t.gameObject.tag == "AudioSourceTemplate")
                        {
                            focusedSource = t.GetComponent<AudioSource>();
                            break;
                        }
                    }
                    if (!focusedSource.isPlaying)
                    {
                        focusedSource.Play();
                    }
                    //Omitted because before the question has been
                    //correctly answered, no infoboxes will be added.
                    foreach (Transform t in focusedObject.transform)
                    {
                        if (t.gameObject.tag == "Infobox")
                        {
                            //Finding VideoQuad in infobox
                            VideoInteractive vdInter = t.GetComponentInChildren<VideoInteractive>();
                            if (vdInter != null)
                            {
                                //If video isn't being played play audio, otherwise stop it
                                if (vdInter.allowOtherAudio)
                                {
                                    if (!focusedSource.isPlaying)
                                    {
                                        focusedSource.Play();
                                    }
                                }
                                else
                                {
                                    focusedSource.Stop();
                                }
                            }
                        }
                    }
                }
            }
            else //if user stops interacting, stop focused object sound, play target object sound
            {
                foreach (GameObject go in activeSoundObjects) //stop all audio other than target
                {
                    if (go != targetSoundObject && go.GetComponent<AudioSource>().isPlaying)
                    {
                        go.GetComponent<AudioSource>().Stop();
                    }
                }
                if (targetSoundObject != null && !targetSource.isPlaying) //Play target if it 
                {                                                         //isn't already playing.                                                                          
                    targetSource.Play();
                }
            }
        }
        else //if wave is over
        {
            if (isFocused)
            {
                if (focusedObject != null)
                {
                    //AudioSource focusedSource = focusedObject.transform.GetChild(0).gameObject.GetComponent<AudioSource>();
                    AudioSource focusedSource = new AudioSource();
                    foreach(Transform t in focusedObject.transform)
                    {
                        if(t.gameObject.tag == "AudioSourceTemplate")
                        {
                            focusedSource = t.GetComponent<AudioSource>();
                            break;
                        }
                    }
                    foreach (GameObject go in activeSoundObjects) //stop all audio other than focusedObject
                    {
                        if (go != focusedSource.gameObject
                                    && go.GetComponent<AudioSource>().isPlaying)
                        {
                            go.GetComponent<AudioSource>().Stop();
                        }
                    }
                    if (!focusedSource.isPlaying)
                    {
                        focusedSource.Play();
                    }
                    //Omitted because before the question has been
                    //correctly answered, no infoboxes will be added.
                    foreach (Transform t in focusedObject.transform)
                    {
                        if (t.gameObject.tag == "Infobox")
                        {
                            //Finding VideoQuad in infobox
                            VideoInteractive vdInter = t.GetComponentInChildren<VideoInteractive>();
                            if (vdInter != null)
                            {
                                //If video isn't being played play audio, otherwise stop it
                                if (vdInter.allowOtherAudio)
                                {
                                    if (!focusedSource.isPlaying)
                                    {
                                        focusedSource.Play();
                                    }
                                }
                                else
                                {
                                    focusedSource.Stop();
                                }
                            }
                        }
                    }
                }
            }
            else //if user stops interacting, stop focused object sound
            {
                if (focusedObject != null)
                {
                    //AudioSource focusedSource = focusedObject.transform.GetChild(0).gameObject.GetComponent<AudioSource>();
                    AudioSource focusedSource = new AudioSource();
                    foreach (Transform t in focusedObject.transform)
                    {
                        if (t.gameObject.tag == "AudioSourceTemplate")
                        {
                            focusedSource = t.GetComponent<AudioSource>();
                            break;
                        }
                    }
                    if (focusedSource.isPlaying)
                        focusedSource.Stop();
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns>Returns an empty GameObject with an AudioSource containing 
    /// a custom rolloff.</returns>
    private GameObject GetSoundObject()
    {
        for (int i = 0; i < GameController.instance.sizeOfPools; i++)
        {
            if (!soundObjectPool[i].activeSelf)
            {
                soundObjectPool[i].SetActive(true);
                return soundObjectPool[i];
            }
        }
        return null;
    }
}
