﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using VRStandardAssets.Utils;

//Issues: Videos freeze while at the same time returning true on isPlaying.
public class VideoInteractive : MonoBehaviour {

    public VideoPlayer videoPlayer; 
    public AudioSource audioSource;
    public VRInteractiveItem interactiveItem;
    public RawImage image;
    public Texture idleThumb, highlightedThumb;
    public bool allowOtherAudio; /*!< When false, audio other than the video's must stop.*/

    private float elapsed;
    private bool startCount;
    private Texture videoTexture;
    private Behaviour interactionScript = null;

    // Use this for initialization
    void Start ()
    {
        //thumb = image.texture;
        allowOtherAudio = true;
        elapsed = 0;
        startCount = false;
        interactiveItem.OnClick += HandleClick;
        interactiveItem.OnOver += HandleOver;
        interactiveItem.OnOut += HandleOut;

        if (transform.parent.parent.parent != null)
        {
            if (GameController.instance.sceneName == "Basic Forest Summer")
            {
                interactionScript =
                        transform.parent.parent.parent.GetComponent<VRInteractiveItemApplied>();
            }
            else if (GameController.instance.sceneName == "Challenge Forest Summer")
            {
                interactionScript =
                        transform.parent.parent.parent.GetComponent<ChallengeVRInteractiveItemApplied>();
            }
            else if (GameController.instance.sceneName == "Intro")
            {
                interactionScript =
                        transform.parent.parent.parent.GetComponent<IntroVRInteractiveItemApplied>();
            }
        }        

        StartCoroutine("InitiateVideo");
	}

    void Update()
    {
        if (startCount)
        {
            elapsed += Time.deltaTime;
            //Eventhough video seems to stop it doesn't always register as such
            //and VideoPlayer.isPlaying continues to return true. This makes sure 
            //that after a duration of time equal to the length of the video has passed 
            //the video is manually stopped. VideoPlayer.Time is not being used
            //because playback can freeze and VideoPlayer.Time will not move forward. 
            /*if (elapsed >= videoPlayer.clip.length && videoPlayer.isPlaying)
            {
                //videoPlayer.Stop();
                startCount = false;
                //image.texture = thumb;
                //Debug.Log("Video stopped.");
            }*/
            if (elapsed >= videoPlayer.clip.length)
            {
                //videoPlayer.Stop();
                startCount = false;
                //image.texture = thumb;
                //Debug.Log("Video stopped.");
            }
        }
        if (!startCount)
        {
            if(image.texture == videoTexture)
            {
                //Debug.Log("Resetting to idle because video over");
                image.texture = highlightedThumb;
                AspectRatio(image);
                allowOtherAudio = true;
                if (GameController.instance.sceneName == "Basic Forest Summer")
                {
                    foreach (GameObject go in AudioController.instance.activeSoundObjects)
                    {
                        go.transform.parent.GetComponent<VRInteractiveItemApplied>().enabled = true;
                    }
                }
                else if (GameController.instance.sceneName == "Challenge Forest Summer")
                {
                    foreach (GameObject go in ChallengeAudioController.instance.activeSoundObjects)
                    {
                        go.transform.parent.GetComponent<ChallengeVRInteractiveItemApplied>().enabled = true;
                    }
                }
                //Enable tree interactability                
                if (interactionScript != null)
                {
                    if (GameController.instance.sceneName == "Challenge Forest Summer"
                    || GameController.instance.sceneName == "Basic Forest Summer")
                    {
                        InterfaceController.instance.checkDist = true;
                    }
                    interactionScript.enabled = true;
                }
            }
        }    
    }

    IEnumerator InitiateVideo()
    {

        //Add VideoPlayer to the GameObject
        //videoPlayer = gameObject.AddComponent<VideoPlayer>();

        //Add AudioSource
        //audioSource = gameObject.AddComponent<AudioSource>();

        //Disable Play on Awake for both Video and Audio
        videoPlayer.playOnAwake = false;
        audioSource.playOnAwake = false;
        audioSource.Pause();

        //We want to play from video clip not from url

        //videoPlayer.source = VideoSource.VideoClip;

        // Vide clip from Url
        //videoPlayer.source = VideoSource.Url;
        //videoPlayer.url = "http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";


        //Set Audio Output to AudioSource
        //videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        videoPlayer.EnableAudioTrack(0, true);
        //videoPlayer.SetTargetAudioSource(0, audioSource);

        //Set video To Play then prepare Audio to prevent Buffering
        //videoPlayer.clip = videoToPlay;
        videoPlayer.Prepare();

        //Wait until video is prepared
        WaitForSeconds waitTime = new WaitForSeconds(1);
        while (!videoPlayer.isPrepared)
        {
            //Debug.Log("Preparing Video");
            //Prepare/Wait for 5 sceonds only
            yield return waitTime;
            //Break out of the while loop after 5 seconds wait
            break;
        }

        //Debug.Log("Done Preparing Video");
        //saving video texture since it somehow gets overriden after the first play.
        videoTexture = videoPlayer.texture;
        AspectRatio(image);
        //videoPlayer.Play();
        //audioSource.Play();
        //Assign the Texture from Video to RawImage to be displayed
        //image.texture = videoPlayer.texture;


        //Play Video
        /*videoPlayer.Play();

        //Play Sound
        audioSource.Play();

        Debug.Log("Playing Video");
        while (videoPlayer.isPlaying)
        {
            Debug.LogWarning("Video Time: " + Mathf.FloorToInt((float)videoPlayer.time));
            yield return null;
        }

        Debug.Log("Done Playing Video");*/
    }

    void HandleClick()
    {
        if (videoPlayer.isPlaying && (ulong)videoPlayer.frame == videoPlayer.frameCount)
        {
            /*Debug.Log("Clicked while playing, time:" +
                       videoPlayer.time +
                       ", " +
                       videoPlayer.frame + "/" + videoPlayer.frameCount);*/
            
            videoPlayer.Stop();
            audioSource.Stop();
            videoPlayer.Play();
            audioSource.Play();
            allowOtherAudio = false;
            if (GameController.instance.sceneName == "Basic Forest Summer")
            {
                foreach (GameObject go in AudioController.instance.activeSoundObjects)
                {
                    go.transform.parent.GetComponent<VRInteractiveItemApplied>().enabled = false;
                }
            }
            else if (GameController.instance.sceneName == "Challenge Forest Summer")
            {
                foreach (GameObject go in ChallengeAudioController.instance.activeSoundObjects)
                {
                    go.transform.parent.GetComponent<ChallengeVRInteractiveItemApplied>().enabled = false;
                }
            }
            //Disable tree interactability            
            if (interactionScript != null)
            {
                if (GameController.instance.sceneName == "Challenge Forest Summer"
                    || GameController.instance.sceneName == "Basic Forest Summer")
                {
                    InterfaceController.instance.checkDist = false;
                }
                interactionScript.enabled = false;
            }
            //InterfaceController.instance.checkDistance = true;
            /*if (videoPlayer.frame == 0)
            {
                elapsed = 0;
                startCount = true;
            }*/
            if (!startCount)
            {
                //Debug.Log("Starting to count from 0");
                elapsed = 0;
                startCount = true;
            }
            //Debug.Log("Added video texture");
            image.texture = videoTexture;
            AspectRatio(image);
            //Uncommenting the above segment allows for video
            //to be restarted before finishing. However, manually stopping
            //the video seems to cause the ap pto crash when running on phone.
        }
        /*if (videoPlayer.isPlaying)
        {
            Debug.Log("Clicked while playing, time:" +
                       videoPlayer.time +
                       ", " +
                       videoPlayer.frame + "/" + videoPlayer.frameCount);
            videoPlayer.Stop();
            audioSource.Stop();
            videoPlayer.Play();
            audioSource.Play();

            //Uncommenting the above segment allows for video
            //to be restarted before finishing. However, manually stopping
            //the video seems to cause the app to crash when running on phone.
        }*/
        else
        {
            /*Debug.Log("Clicked while stopped, time:" + 
                       videoPlayer.time + 
                       ", " +
                       videoPlayer.frame + "/" + videoPlayer.frameCount);*/
            
            //image.texture = videoPlayer.texture;
            /*if (!videoPlayer.isPlaying)
            {
                Debug.Log("Starting count");
                elapsed = 0;
                startCount = true;                
            }*/
            videoPlayer.Play();
            audioSource.Play();
            if (!startCount)
            {
                //Debug.Log("Starting to count from 0");
                elapsed = 0;
                startCount = true;
            }
            allowOtherAudio = false;
            if(GameController.instance.sceneName == "Basic Forest Summer")
            {
                foreach(GameObject go in AudioController.instance.activeSoundObjects)
                {
                    go.transform.parent.GetComponent<VRInteractiveItemApplied>().enabled = false;
                }
            }
            else if (GameController.instance.sceneName == "Challenge Forest Summer")
            {
                foreach (GameObject go in ChallengeAudioController.instance.activeSoundObjects)
                {
                    go.transform.parent.GetComponent<ChallengeVRInteractiveItemApplied>().enabled = false;
                }
            }
            //Disable tree interactability            
            if (interactionScript != null)
            {
                if(GameController.instance.sceneName == "Challenge Forest Summer"
                    || GameController.instance.sceneName == "Basic Forest Summer")
                {
                    InterfaceController.instance.checkDist = false;
                }                
                interactionScript.enabled = false;
            }
            //InterfaceController.instance.checkDistance = false;
            image.texture = videoTexture;
            AspectRatio(image);
            //Debug.Log("Added video texture");
        }
    }

    void HandleOver()
    {
        if (!videoPlayer.isPlaying)
        {
            //Debug.Log("Setting to highlighted");
            image.texture = highlightedThumb;
            AspectRatio(image);
        }        
    }

    void HandleOut()
    {
        if (!videoPlayer.isPlaying)
        {
            //Debug.Log("Resetting to idle because handle out");
            image.texture = idleThumb;
            AspectRatio(image);
        }
    }

    /// <summary>
    /// Changes image aspect ratio to that of texture.
    /// </summary>
    /// <param name="raw"></param>
    void AspectRatio(RawImage raw)
    {
        //Note: x component of raw.rectTransform.sizeDelta is width
        raw.SetNativeSize();
        float height;
        float width;
        height = raw.rectTransform.sizeDelta.y;
        width = raw.rectTransform.sizeDelta.x;
        //Debug.Log(raw.rectTransform.sizeDelta);

        float ratio = width / height;

        float newWidth = ratio * 100;

        raw.rectTransform.sizeDelta = new Vector2(newWidth, 100);
    }
}
