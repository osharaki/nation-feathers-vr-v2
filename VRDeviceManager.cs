﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class VRDeviceManager : MonoBehaviour {


    [SerializeField] private float RenderScale;              //The render scale. Higher numbers = better quality, but trades performance
    [SerializeField] private int AA;


    void Start () {
        VRSettings.renderScale = RenderScale;
        QualitySettings.antiAliasing = AA;        
    }
}
