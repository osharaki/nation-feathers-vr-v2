﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Restricts the lapCounter's (a quad) movement. 
/// This way, the ball will hit the counter at roughly the same position every time.
/// Also, counts how many laps the bird has done.
/// </summary>
public class MovementRestriction : MonoBehaviour
{
    public GameObject player;
    public GameObject CirclingBird;

    //Vector3 orig;
    //Quaternion origRot;
    float offset;

    void Start()
    {
        //orig = transform.position;
        //origRot = transform.rotation;
        //offset = GetComponent<Renderer>().bounds.size.x / 2;
        offset = transform.localScale.x / 2;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.transform.position.x,
                                         CirclingBird.transform.position.y,
                                         player.transform.position.z + offset);
        //transform.rotation = origRot;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Circling Bird")
        {
            CirclingBird.GetComponent<MovingBird>().laps++;
            //Debug.Log(CirclingBird.GetComponent<MovingBird>().laps);
        }
    }
}