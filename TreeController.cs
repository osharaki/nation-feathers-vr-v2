﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeController : MonoBehaviour {
    
    public float offset;
    public GameObject dustEffect;


    GameObject player;
    bool addedDustEffect = false;
    GameObject dustEffectInstance;
    //GenerateTerrain generateTerrainScript;

    void Start()
    {
        player = GameObject.FindWithTag("Player");
        if (GetComponent<AudioSource>() != null)
        {
            dustEffectInstance = Instantiate(dustEffect, //add dust effect
                        new Vector3(transform.position.x, transform.position.y - 5, transform.position.z),
                        Quaternion.identity);
            dustEffectInstance.transform.localScale = new Vector3(5, 5, 5); //enlarge effect to make it more visible
            dustEffectInstance.SetActive(false);
        }
        //generateTerrainScript = GetComponentInParent<GenerateTerrain>();
    }
    // Update is called once per frame
    void Update () {
        if (!addedDustEffect)   //if dust effect hasn't been added to tree
        {
            if (GetComponent<AudioSource>() != null)    //if tree has AudioSource attached
            {
                if ((player.transform.position.x <= transform.position.x + offset    //if player is in close proximity to tree
                    && player.transform.position.x >= transform.position.x - offset)
                    && (player.transform.position.z <= transform.position.z + offset
                    && player.transform.position.z >= transform.position.z - offset))
                {
                    dustEffectInstance.SetActive(true);
                    addedDustEffect = true;                    
                }
            }
        }
        else //if player moves away remove effect
        {            
            if (player.transform.position.x > transform.position.x + offset    //if player is away from tree
                    || player.transform.position.x < transform.position.x - offset
                    || player.transform.position.z > transform.position.z + offset
                    || player.transform.position.z < transform.position.z - offset)
            {
                dustEffectInstance.SetActive(false);
                addedDustEffect = false;
            }
        }       	    
	}
}
