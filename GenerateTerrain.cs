﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityScript.Lang;

/// <summary>
/// Uses Perlin Noise to create the hilly terrain on smart planes. Keeps track of the plane's child objects 
/// such as food and trees.
/// </summary>

[ExecuteInEditMode]
public class GenerateTerrain : MonoBehaviour {

    /*public static class Shuffler
    {
        public static void Shuffle<T>(this IList<T> list, Random rnd)
        {
            for (var i = 0; i < list.Count; i++)
                list.Swap(i, rnd.Next(i, list.Count));
        }

        public static void Swap<T>(this IList<T> list, int i, int j)
        {
            var temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }
    }*/

    public int heightScale; /*!< How high the terrain will be.*/
    public float detailScale;   /*!< Adjusts smoothness of the terrain.*/    
	public float treeDepth; /*!< A value applied to trees to bury them a certain distance in the plane in order to cover their base.*/
    public int treesPerPlane;   /*!< Amount of trees per plane.*/																																				  
    //public GameObject[] treePrefabs;
    public float elevation;
    public AudioClip[] birdSounds;
    public bool addSound = false;   /*!< Value set by GenerateInfiniteFPV to true if plane number is a multiple of 10.*/    
    public float minDist;
    public int edgeDist; /*!< Minimum distance trees will have from edge of terrain.*/

    public int totalTrees;    //total number of trees in the pool
    public GameObject[] treePrefabs;    /*!< This array contains the different tree objects to be chosen from randomly for the pool.*/
    public bool generateNewTrees = false;
    static GameObject[] trees;

    private bool isSinglePlane;
    //private List<GameObject> myTrees = new List<GameObject>(); /*!< List of trees situated on plane.*/    
    private bool toBeDestroyed;
    Vector3 treePos;
    Vector3[] vertices;
    Terrain thisTerrain;
    Vector3[] posOnTerrain;
    int heightMapRes;
    float[,] heights;
    float sizeofTerrX, sizeofTerrZ;

    // Use this for initialization
    /*void Start () {
         
        thisTerrain = GetComponent<Terrain>();
        if(thisTerrain != null)
        {
            heightMapRes = thisTerrain.terrainData.heightmapResolution;
            heights = thisTerrain.terrainData.GetHeights(0, 0,
                                                       heightMapRes,
                                                       heightMapRes);
            //sizeofTerrX = thisTerrain.terrainData.size.x;
            //sizeofTerrZ = thisTerrain.terrainData.size.z;
            sizeofTerrX = heightMapRes;
            sizeofTerrZ = heightMapRes;
            posOnTerrain = new Vector3[(int)((int)((sizeofTerrX + 1) - (edgeDist * 2)) 
                                           * (int)((sizeofTerrZ + 1) - (edgeDist * 2)))];
            int inc = 0;
            for (int x = 0; x <= (int)(sizeofTerrX - edgeDist * 2); x++)
            {
                for(int z = 0; z <= (int)(sizeofTerrZ - edgeDist * 2); z++)
                {
                    posOnTerrain[inc] = new Vector3(x + edgeDist, 
                                                    heights[x, z], 
                                                    z + edgeDist);
                    //Debug.Log(posOnTerrain[inc]);
                    inc++;
                }                
            }
            isSinglePlane = true;
            //Debug.Log(heights[0, 0]);
        }
        else
        {
            isSinglePlane = false;
        }            

        treePos = Vector3.zero;

        //ManipulateTerrain();
        if (TreePool.Instance != null)
        {
            AddTrees();
        }
        else
            Debug.Log("Can't find treePool. No trees will be added.");
    }*/

    /// <summary>
    /// Adds AudioSource component to a game object and adds a sound clip to the AudioSource.
    /// </summary>
    IEnumerator AddSound(AudioClip[] audioClipArray, GameObject targetGameObject)
    {
        //Debug.Log("AddSound function has been called");
        AudioSource audioSourceComponent = targetGameObject.AddComponent<AudioSource>();
        //Debug.Log("AudioSource has been added");
        audioSourceComponent.clip = audioClipArray[Random.Range(0, audioClipArray.Length - 1)];
        audioSourceComponent.spatialBlend = 1;
        audioSourceComponent.loop = true;
        audioSourceComponent.volume = 1;
        audioSourceComponent.maxDistance = (GenerateInfiniteFPV.Instance.halfTilesZ * 100) / 20;
        audioSourceComponent.rolloffMode = AudioRolloffMode.Custom;
        yield return new WaitForSeconds(Random.Range(0, 2));    //Start to play sound at different offsets
        //Debug.Log("sound will play now");
        audioSourceComponent.Play();
    }

    void ManipulateTerrain()
    {
        float max = 0;

        if (!isSinglePlane)
        {
            Mesh mesh = GetComponent<MeshFilter>().mesh;
            vertices = mesh.vertices;
            for (int v = 0; v < vertices.Length; v++)
            {
                //Debug.Log(transform.TransformPoint(vertices[v]));
                vertices[v].y = Mathf.PerlinNoise((vertices[v].x + transform.position.x) / detailScale,
                                    (vertices[v].z + transform.position.z) / detailScale) * heightScale;
                if (max < vertices[v].y)
                {
                    max = vertices[v].y;
                }
            }
            mesh.vertices = vertices;
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            gameObject.AddComponent<MeshCollider>();            
        }
        else //if a single piece of terrain is used
        {            
            //Debug.Log(transform.TransformVector(thisTerrain.terrainData.size));


            float[,] heights = thisTerrain.terrainData.GetHeights(0, 0,
                                                       heightMapRes,
                                                       heightMapRes);
            int posOnTerrainIter = 0;
            for (int x = 0; x < (int)(sizeofTerrX - edgeDist * 2); x++)
            {
                for (int z = 0; z < (int)(sizeofTerrZ - edgeDist * 2); z++)
                {
                    //Debug.Log(transform.TransformPoint(x, heights[x, z], z));
                    /*float heightAtPoint = thisTerrain.SampleHeight(new Vector3(transform.position.x + x,
                                                                               transform.position.y,
                                                                               transform.position.z + z));*/
                    /*float perlinX = ((int)(transform.position.x + (x % 10) 
                                    / thisTerrain.terrainData.size.x)) 
                                    * 10;
                    float perlinZ = ((int)(transform.position.z + (z % 10) 
                                    / thisTerrain.terrainData.size.z))
                                    * 10;*/
                    float newHeight = (Mathf.PerlinNoise(x / detailScale,
                                                         z / detailScale) * heightScale) / 10000;
                    posOnTerrain[posOnTerrainIter] = new Vector3(x, newHeight, z);
                    if (!toBeDestroyed)
                    {
                        heights[x, z] = newHeight;
                    }
                    else
                    {
                        heights[x, z] = 0;
                    }
                    posOnTerrainIter++;                       
                }
            }
            thisTerrain.terrainData.SetHeights(0, 0, heights);
            thisTerrain.terrainData.RefreshPrototypes();
            thisTerrain.Flush();
        }
    }

    /*void AddTrees()
    {
        if (!isSinglePlane)
        {
            int treesPlaced = 0;
            Vector3[] sortedVertices = vertices.OrderBy(v => v.y).ToArray<Vector3>(); //sorted from smallest to biggest
                                                                                      //creating n at highest n vertices in plane: Where n=treesPerPlane  
            for (int i = 0; i < treesPerPlane; i++)
            {
                //GameObject tree = Instantiate(treePrefabs[Random.Range(0, treePrefabs.Length)], Vector3.zero, Quaternion.Euler(-90, 0, 0));
                GameObject tree = TreePool.getTree();
                if (tree != null)
                {
                    treePos = new Vector3(sortedVertices[(vertices.Length - 1) - (i + 40)].x + transform.position.x,
                                        sortedVertices[(vertices.Length - 1) - i].y + elevation,
                                            sortedVertices[(vertices.Length - 1) - (i + 40)].z + transform.position.z);
                    tree.transform.position = treePos;
                    tree.transform.parent = transform;
                    //tree.GetComponent<Renderer>().enabled = true;
                    treesPlaced++;
                    if (addSound)
                    {
                        //Debug.Log("AudioSource will be added to tile: " + name);
                        StartCoroutine(AddSound(birdSounds, tree));
                    }
                    myTrees.Add(tree);
                }
            }
        }
        else
        {            
            int sizeX = (int)thisTerrain.terrainData.size.x;
            int sizeZ = (int)thisTerrain.terrainData.size.z;
            float[,] heights = thisTerrain.terrainData.GetHeights(0, 0,
                                                       heightMapRes,
                                                       heightMapRes);
                        
            List<Vector3> triedPos = new List<Vector3>();
            GameObject tree;

            //int randIndex = Random.Range(0, posOnTerrain.Length);
            while ((tree = TreePool.getTree()) != null)
            {                                
                new_randIndex:
                //Generate a new randIndex and check if we've used that pos before
                int randIndex = Random.Range(0, posOnTerrain.Length);
                if (triedPos.Contains<Vector3>(posOnTerrain[randIndex])) //if pos has been tried, generate new randIndex
                {
                    goto new_randIndex;
                }
                else //if this pos hasn't been tried yet 
                {
                    //Are there trees close to this pos
                    foreach (GameObject aTree in myTrees)
                    {
                        //If dist bet. trees is too small go back to very beginning and generate new randIndex
                        if (Vector3.Distance(aTree.transform.position, posOnTerrain[randIndex]) < minDist)
                        {
                            triedPos.Add(posOnTerrain[randIndex]);
                            goto new_randIndex; //generate new randIndex
                        }
                    }
                    //If we reach here it means the pos at randIndex is good
                    triedPos.Add(posOnTerrain[randIndex]);
                    BoxCollider treeColl = tree.GetComponent<BoxCollider>();
                    if(treeColl != null)
                    {
                        elevation = treeColl.bounds.size.y / 2;
                    }
                    else
                    {
                        Debug.Log("Tree has no BoxCollider");
                        elevation = 0;
                    }
                    //Vector3 terrainToWorld = posOnTerrain[randIndex];
                    //transform.TransformPoint(terrainToWorld);

                    //Setting the appropriate elevation for each tree 
                    //Vector3 rendererSize = Vector3.zero;
                    //foreach (Transform child in tree.transform)
                    //{
                        //if(child.tag == "Boxy")
                        //{
                            //rendererSize = child.GetComponent<Renderer>().bounds.size;
                            //Debug.Log(tree.name + "rendererSize: " + rendererSize);
                            //Debug.Log(tree.name + "Center: " + transform.TransformPoint(child.GetComponent<Renderer>().bounds.center));
                            //rendererSize = new Vector3(rendererSize.x, rendererSize.y - child.GetComponent<Renderer>().bounds.center.y, rendererSize.z);
                            //Debug.Log(tree.name + "rendererSize.y - center.y: " + rendererSize);
                        //}
                    //}

                    //elevation = rendererSize.y * 0.5f;

                    treePos = new Vector3(((float)sizeX / heightMapRes) * posOnTerrain[randIndex].x + transform.position.x,
                                          posOnTerrain[randIndex].y + elevation,
                                          ((float)sizeZ / heightMapRes) * posOnTerrain[randIndex].z + transform.position.z);

                    tree.transform.position = treePos;
                    //Debug.Log("posOnTerrain: " + posOnTerrain[randIndex]);
                    //Debug.Log("TerrainToWorld: " + terrainToWorld);
                    //Debug.Log("treePos: " + treePos);                                        
                    foreach(Renderer r in tree.GetComponentsInChildren<Renderer>())
                    {
                        r.enabled = true;
                    }
                    myTrees.Add(tree);                    
                }                
            }
            //GameObject[] boxes = GameObject.FindGameObjectsWithTag("Boxy");
            //foreach(GameObject box in boxes)
            //{
                //Debug.Log(box.GetComponent<Renderer>().bounds.size);
            //}
        }
    }*/
    public void AddTrees()
    {
        thisTerrain = GetComponent<Terrain>();
        heightMapRes = thisTerrain.terrainData.heightmapResolution;
        heights = thisTerrain.terrainData.GetHeights(0, 0,
                                                   heightMapRes,
                                                   heightMapRes);
        //sizeofTerrX = thisTerrain.terrainData.size.x;
        //sizeofTerrZ = thisTerrain.terrainData.size.z;
        sizeofTerrX = thisTerrain.terrainData.size.x;
        sizeofTerrZ = thisTerrain.terrainData.size.z;
        posOnTerrain = new Vector3[(int)((int)((sizeofTerrX) - (edgeDist * 2))
                                       * (int)((sizeofTerrZ) - (edgeDist * 2)))];
        int inc = 0;
        for (int x = 0; x < (int)(sizeofTerrX - edgeDist * 2); x++)
        {
            for (int z = 0; z < (int)(sizeofTerrZ - edgeDist * 2); z++)
            {
                posOnTerrain[inc] = new Vector3(x + edgeDist + transform.position.x,
                                                heights[x, z],
                                                z + edgeDist + transform.position.z);
                //Debug.Log(posOnTerrain[inc]);                
                inc++;                
            }
        }
        /*int k = 0;        
        foreach (Vector3 v in posOnTerrain)
        {
            if (v.x == 0 && v.z == 0)
                Debug.Log(k);
            k++;
        }*/       

        isSinglePlane = true;
        treePos = Vector3.zero;
        if (generateNewTrees)
        {
            trees = new GameObject[totalTrees];
            for (int i = 0; i < totalTrees; i++)
            {
                int randIndex = Random.Range(0, treePrefabs.Length);
                Quaternion treePrefabAngle = treePrefabs[randIndex].transform.rotation;
                trees[i] = Instantiate(treePrefabs[randIndex], Vector3.zero, Quaternion.Euler(treePrefabAngle.eulerAngles.x, treePrefabAngle.eulerAngles.y, treePrefabAngle.eulerAngles.z));
                trees[i].transform.parent = transform;
                foreach (Renderer r in trees[i].GetComponentsInChildren<Renderer>())
                {
                    r.enabled = false;
                }
            }
        }
        List<GameObject> myTrees = new List<GameObject>();
        //Debug.Log(heights[0, 0]);
        if (!isSinglePlane)
        {
            int treesPlaced = 0;
            Vector3[] sortedVertices = vertices.OrderBy(v => v.y).ToArray<Vector3>(); //sorted from smallest to biggest
                                                                                      //creating n at highest n vertices in plane: Where n=treesPerPlane  
            for (int i = 0; i < treesPerPlane; i++)
            {
                //GameObject tree = Instantiate(treePrefabs[Random.Range(0, treePrefabs.Length)], Vector3.zero, Quaternion.Euler(-90, 0, 0));
                GameObject tree = getTree();
                if (tree != null)
                {
                    treePos = new Vector3(sortedVertices[(vertices.Length - 1) - (i + 40)].x + transform.position.x,
                                        sortedVertices[(vertices.Length - 1) - i].y + elevation,
                                            sortedVertices[(vertices.Length - 1) - (i + 40)].z + transform.position.z);
                    tree.transform.position = treePos;
                    tree.transform.parent = transform;
                    //tree.GetComponent<Renderer>().enabled = true;
                    treesPlaced++;
                    if (addSound)
                    {
                        //Debug.Log("AudioSource will be added to tile: " + name);
                        StartCoroutine(AddSound(birdSounds, tree));
                    }
                    myTrees.Add(tree);
                }
            }
        }
        else
        {
            int sizeX = (int)thisTerrain.terrainData.size.x;
            int sizeZ = (int)thisTerrain.terrainData.size.z;
            float[,] heights = thisTerrain.terrainData.GetHeights(0, 0,
                                                       heightMapRes,
                                                       heightMapRes);
            //Initializing the list where the random number will be coming from.
            List<int> aList = new List<int>();
            for (int i = 0; i < posOnTerrain.Length; i+=(int)minDist)
            {
                aList.Add(i);
            }

            List<Vector3> triedPos = new List<Vector3>();
            GameObject tree;
            int iter = GetRandom(aList);
            tree = getTree();
            bool noTrees = false;
            bool noPlaces = false;
            //int randIndex = Random.Range(0, posOnTerrain.Length);
            while (!noTrees && !noPlaces)
            {
                if(tree != null)
                {
                    //Generate a new randIndex and check if we've used that pos before                
                    //Are there trees close to this pos
                    foreach (GameObject aTree in myTrees)
                    {
                        //If dist bet. trees is too small go back to very beginning and generate new randIndex
                        if (Vector3.Distance(aTree.transform.position, posOnTerrain[iter]) < minDist)
                        {
                            //Debug.Log(aTree.transform.position + "..." + posOnTerrain[iter]);
                            triedPos.Add(posOnTerrain[iter]);
                            goto new_randIndex; //generate new randIndex
                        }
                    }
                    //If we reach here it means the pos at randIndex is good
                    triedPos.Add(posOnTerrain[iter]);
                    //BoxCollider treeColl = tree.GetComponent<BoxCollider>();
                    //if (treeColl != null)
                    //{
                       // elevation = treeColl.bounds.size.y / 2;
                    //}
                    //else
                    //{
                        //Debug.Log("Tree has no BoxCollider");
                        //elevation = 0;
                    //}
                    //Vector3 terrainToWorld = posOnTerrain[randIndex];
                    //transform.TransformPoint(terrainToWorld);

                    //Setting the appropriate elevation for each tree 
                    //Vector3 rendererSize = Vector3.zero;
                    /*foreach (Transform child in tree.transform)
                    {
                        if(child.tag == "Boxy")
                        {
                            rendererSize = child.GetComponent<Renderer>().bounds.size;
                            Debug.Log(tree.name + "rendererSize: " + rendererSize);
                            Debug.Log(tree.name + "Center: " + transform.TransformPoint(child.GetComponent<Renderer>().bounds.center));
                            rendererSize = new Vector3(rendererSize.x, rendererSize.y - child.GetComponent<Renderer>().bounds.center.y, rendererSize.z);
                            Debug.Log(tree.name + "rendererSize.y - center.y: " + rendererSize);
                        }
                    }*/

                    //elevation = rendererSize.y * 0.5f;

                    treePos = new Vector3(posOnTerrain[iter].x,
                                            0,
                                            posOnTerrain[iter].z);
                    /*if (treePos == Vector3.zero)
                        Debug.Log("Item " + iter + " in posOnTerrain is faulty.");*/
                    tree.transform.position = treePos;
                    
                    //Debug.Log("posOnTerrain: " + posOnTerrain[randIndex]);
                    //Debug.Log("TerrainToWorld: " + terrainToWorld);
                    //Debug.Log("treePos: " + treePos);                                        
                    foreach (Renderer r in tree.GetComponentsInChildren<Renderer>())
                    {
                        r.enabled = true;
                    }
                    myTrees.Add(tree);
                    //Debug.Log("Tree added!");
                    tree = getTree();
                }    
                else
                {
                    noTrees = true;
                    //continue;
                }
                
                new_randIndex:
                iter = GetRandom(aList);
                if(iter == -1)
                {
                    noPlaces = true;
                }
            }
            /*GameObject[] boxes = GameObject.FindGameObjectsWithTag("Boxy");
            foreach(GameObject box in boxes)
            {
                Debug.Log(box.GetComponent<Renderer>().bounds.size);
            }*/
        }
    }

    /*void OnDestroy()
    {
        toBeDestroyed = true;
        if (isSinglePlane)  //Only changes made to terrain need to be reverted. A plane reverts on its own
        {
            ManipulateTerrain();
        }        
    }*/

    /// <summary>
    /// From a list of integers chooses a random number.
    /// Never returns the same number again.
    /// </summary>
    /// <param name="aList"></param>
    /// <returns>Unique random number from an integer list.</returns>
    int GetRandom(List<int> aList)
    {
        if(aList.Count == 0)
        {
            return -1;
        }
        int randInd = Random.Range(0, aList.Count);
        int result = aList[randInd];
        aList.RemoveAt(randInd);
        return result;
    }

    GameObject getTree()
    {
        for (int i = 0; i < totalTrees; i++)
        {
            foreach (Renderer r in trees[i].GetComponentsInChildren<Renderer>())
            {
                if (r.enabled)
                    goto iteration_end;
            }
            return trees[i];

            iteration_end:;
        }
        return null;
    }
}
