﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the behaviour of a quad used to fade out soundscape
/// audio in intro scene.
/// </summary>
public class SoundscapeQuadHandler : MonoBehaviour {

    [SerializeField] private List<AudioSource> AudioSourceList; /*!< Audio sources in soundscape.*/
    [SerializeField] private Transform treePathContainer;
    [SerializeField] private Transform billboardQuads;
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("entered");
        StartCoroutine("AudioSourceFade");
        treePathContainer.GetComponent<BillboardParent>().enabled = false;
        //billboardQuads.GetComponent<BillboardParent>().enabled = true;
        /*foreach(Transform t in treePathContainer)
        {
            t.gameObject.GetComponent<CameraFacingBillboard>().enabled = false;
        }*/
    }

    /// <summary>
    /// Fades AudioSource volumes out then disables the game object
    /// this script is attache to.
    /// </summary>
    /// <returns></returns>
    private IEnumerator AudioSourceFade()
    {
        float currentVolume = 1;
        float t = 0;
        while(currentVolume != 0)
        {
            t += 0.005f * Time.deltaTime; 
            foreach (AudioSource audioSource in AudioSourceList)
            {
                currentVolume = Mathf.Lerp(audioSource.volume, 0, t);
                
                if (currentVolume <= 0.001)
                {
                    currentVolume = 0;                    
                }
                audioSource.volume = currentVolume;
            }
            
            yield return null;
        }
        gameObject.SetActive(false);
    }
}
