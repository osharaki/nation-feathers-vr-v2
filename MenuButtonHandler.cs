﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRStandardAssets.Utils;

/// <summary>
/// Handles actions performed when menu buttons are clicked.
/// </summary>
public class MenuButtonHandler : MonoBehaviour {

    public enum Button
    {
        INTRO,
        REPLAY_DAY,
        REPLAY_CHALLENGE
    }

    public VRInteractiveItem interactiveItem;    
    public Button myButton;
    public Camera myCamera;
    public Animator animator;

    private RawImage image;
    // Use this for initialization
    void Start () {
        image = gameObject.GetComponentInChildren<RawImage>();
        myCamera = Camera.main;
        interactiveItem.OnClick += HandleClick;
        interactiveItem.OnOver += HandleOver;
        interactiveItem.OnOut += HandleOut;
    }
	
	void HandleClick()
    {
        if(myButton == Button.INTRO)
        {
            //Replay the intro
            GameController.instance.menuSelection = GameController.MenuSelection.INTRO;
            myCamera.GetComponent<VRCameraFade>().FadeOut(false);
        }
        else if(myButton == Button.REPLAY_DAY)
        {
            //Replay day scene
            GameController.instance.menuSelection = GameController.MenuSelection.REPLAY_DAY;
            myCamera.GetComponent<VRCameraFade>().FadeOut(false);
        }
        else if(myButton == Button.REPLAY_CHALLENGE)
        {
            //Replay challenge scene
            GameController.instance.menuSelection = GameController.MenuSelection.REPLAY_CHALLENGE;
            myCamera.GetComponent<VRCameraFade>().FadeOut(false);
        }
    }

    void HandleOver()
    {
        //Debug.Log("We are over");
        animator.Play("Enlarge", 0);                
    }

    void HandleOut()
    {
        animator.Play("Idle", 0);
    }
}
