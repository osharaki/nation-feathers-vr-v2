﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

/// <summary>
/// Dictates the behaviour of interactive objects such as trees
/// in the intro scene.
/// </summary>
public class IntroVRInteractiveItemApplied : MonoBehaviour {

    public VRInteractiveItem interactiveItem;

    private GameObject thisDustContainer;
    private SpriteRenderer animatedBird;
    private Reticle r;
    private void Start()
    {
        r = Camera.main.GetComponent<Reticle>();
        if (interactiveItem != null)
        {
            interactiveItem.OnOver += HandleOver;
            interactiveItem.OnOut += HandleOut;
            interactiveItem.OnClick += HandleClick;
        }
        foreach (SpriteRenderer sr in GetComponentsInChildren<SpriteRenderer>(true))
        {
            if (sr.gameObject.tag == "Animated_Bird")
            {                
                animatedBird = sr;
                animatedBird.GetComponent<CameraFacingBillboard>().ObjectToLookAt = gameObject;
                animatedBird.GetComponent<CirclingBird>().pivot = gameObject;
            }
        }
        //thisDustContainer = IntroInterfaceController.instance.dustContainer;
    }

    private void OnEnable()
    {
        if (interactiveItem != null)
        {
            interactiveItem.OnOver += HandleOver;
            interactiveItem.OnOut += HandleOut;
            interactiveItem.OnClick += HandleClick;
        }
    }

    private void OnDisable()
    {
        if (interactiveItem != null)
        {
            interactiveItem.OnOver -= HandleOver;
            interactiveItem.OnOut -= HandleOut;
            interactiveItem.OnClick -= HandleClick;
        }
    }

    private void HandleOver()
    {
        //Debug.Log("Object is highlighted");
        /*if (thisDustContainer != null)
        {
            IntroInterfaceController.instance.ActivateDust(thisDustContainer);
        }*/
        if (GameController.instance.sceneEnum == GameController.SceneName.INTRO)
            IntroInterfaceController.instance.gazed = true;
        
        if (r != null)
        {
            r.Show(); //Activate large reticle
        }

        if(GameController.instance.narrative == GameController.NarrativeState.HIGHLIGHTED
            || GameController.instance.narrative == GameController.NarrativeState.LANDED)
        {            
            animatedBird.gameObject.SetActive(true);
        }
        
    }

    private void HandleOut()
    {
        //Debug.Log("Object is NOT highlighted");
        /*if (thisDustContainer != null)
        {
            IntroInterfaceController.instance.DeactivateDust(thisDustContainer);
        }*/
        
        if (r != null)
        {
            r.Hide(); //Deactivate large reticle
        }

        if (GameController.instance.narrative == GameController.NarrativeState.HIGHLIGHTED
            || GameController.instance.narrative == GameController.NarrativeState.LANDED)
        {
            animatedBird.gameObject.SetActive(false);
        }
    }

    public void HandleClick()
    {
        if(animatedBird.gameObject != null && animatedBird.enabled == true)
            animatedBird.gameObject.SetActive(false);
        if (IntroAudioController.instance.isFocused)
        {
            IntroAudioController.instance.isFocused = !IntroAudioController.instance.isFocused;
            IntroInterfaceController.instance.ManageInfobox();
            IntroAudioController.instance.focusedObject = gameObject;
        }
        else
        {
            IntroAudioController.instance.isFocused = !IntroAudioController.instance.isFocused;
            IntroAudioController.instance.focusedObject = gameObject;            
            IntroInterfaceController.instance.ManageInfobox();            
        }
    }
}
