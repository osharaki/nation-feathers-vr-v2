﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the action performed on collision with the quad 
/// controlling the UI arrow.
/// </summary>
public class GateArrowCollisionHandler : MonoBehaviour {


    /// <summary>
    /// Disables GatewayArrow when player walks over it.
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        transform.parent.gameObject.SetActive(false);
    }
}
