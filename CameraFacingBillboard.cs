﻿using UnityEngine;
using System.Collections;

/// <summary>
/// When attched to an object, this scrip rotates it so that it is 
/// always facing the player dpeending on the player's position.
/// </summary>
public class CameraFacingBillboard : MonoBehaviour
{
    public Camera cameraToLookAt;
    public GameObject ObjectToLookAt;
    public float fixedXRot,
                 fixedZRot;
    public bool xFree,
                faceCamera = true;

    void Start()
    {
        cameraToLookAt = Camera.main;
    }
    /*void Start()
    {
        transform.rotation = Quaternion.LookRotation(transform.position - 
            cameraToLookAt.transform.position); //rotates the billboard
        //locks rotation on x-z-axes
        transform.rotation = Quaternion.Euler(fixedXRot, 
                                              transform.rotation.eulerAngles.y, 
                                              fixedZRot); 
    }*/

    void Update()
    {
        if (faceCamera)
        {
            if (!xFree) //if x bilboard not free to move on x axis
            {
                transform.rotation = Quaternion.LookRotation(transform.position -
                cameraToLookAt.transform.position); //rotates the billboard
                                                    //locks rotation on x-z-axes
                transform.rotation = Quaternion.Euler(fixedXRot,
                                                      transform.rotation.eulerAngles.y,
                                                      fixedZRot);
            }
            else
            {
                transform.rotation = Quaternion.LookRotation(transform.position -
                cameraToLookAt.transform.position); //rotates the billboard
                                                    //locks rotation on x-z-axes
                transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x,
                                                      transform.rotation.eulerAngles.y,
                                                      fixedZRot);
            }
        }
        else
        {
            if (!xFree) //if x bilboard not free to move on x axis
            {
                transform.rotation = Quaternion.LookRotation(transform.position -
                ObjectToLookAt.transform.position); //rotates the billboard
                                                    //locks rotation on x-z-axes
                transform.rotation = Quaternion.Euler(fixedXRot,
                                                      transform.rotation.eulerAngles.y,
                                                      fixedZRot);
            }
            else
            {
                transform.rotation = Quaternion.LookRotation(transform.position -
                ObjectToLookAt.transform.position); //rotates the billboard
                                                    //locks rotation on x-z-axes
                transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x,
                                                      transform.rotation.eulerAngles.y,
                                                      fixedZRot);
            }
        }                
    }
    /*void Update()
    {
        //transform.LookAt(cameraToLookAt.transform);
        Vector3 vec = new Vector3()
        transform.LookAt(2 * transform.position - cameraToLookAt.transform.position);
        //aCube.transform.position = 2 * transform.position - cameraToLookAt.transform.position;
    }*/
}