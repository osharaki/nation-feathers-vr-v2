﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Mono.Data.SqliteClient;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Video;

/// <summary>
/// Controls all data flow to/from database.
/// </summary>
public class DBController : MonoBehaviour {

    public static DBController instance;
    public List<AudioClip> AudioFiles; /*!< Audioclips used in game.*/
    public List<Sprite> ImageFiles; /*!< Info box image files.*/
    public List<VideoClip> VideoFiles; /*!< Spectrograms.*/
    public string currentBirdName;

    private IDbConnection dbconn;
    private string conn;
    private string filePath;    

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        //AudioFiles = new List<AudioSource>();        
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            conn = "URI=file:" + Application.dataPath + "/StreamingAssets/Unity_NFDB.db"; //Path to database.
            //Debug.Log(conn);
            //Debug.Log(Application.persistentDataPath + "/Unity_NFDB.db");
        }
        else
        {
            filePath = Application.persistentDataPath + "/Unity_NFDB.db";
            //Debug.Log("osharaki: " + filePath);
            /*if (!File.Exists(filePath))
            {
                Debug.Log("osharaki: " + "filePath doesn't exist.");
                Debug.Log("osharaki: " + "Looking in: " +
                    "jar:file://" + Application.dataPath +
                                "!/assets/Unity_NFDB.db");
                WWW loadDB = new WWW("jar:file://" + Application.dataPath +
                                "!/assets/Unity_NFDB.db");
                while (!loadDB.isDone)
                {

                }

                File.WriteAllBytes(filePath, loadDB.bytes);
                Debug.Log("osharaki: " + "written all bytes");
            }*/
            WWW loadDB = new WWW("jar:file://" + Application.dataPath +
                                "!/assets/Unity_NFDB.db");
            while (!loadDB.isDone)
            {

            }

            File.WriteAllBytes(filePath, loadDB.bytes);
            conn = "URI=file:" + filePath;
        }                      
    }

    /// <summary>
    /// Using parameters retrived from the GameController, this function searches the 
    /// DB for the names of audio files/clips relevant to the scene. Using these names, the function 
    /// then returns an AudioClip list containing audio that is suitable.
    /// </summary>
    /// <returns>AudioSource list containing audio files relevant to the current scene.</returns>
    public List<AudioClip> GetASound()  //does this need to be static? Aparently it doesn't.
    {
        dbconn = (IDbConnection)new SqliteConnection(conn);
        dbconn.Open(); //Open connection to the database

        string TOD = GameController.currentTOD.ToString();
        string season = GameController.currentSeason.ToString();
        string habitat = GameController.currentTerrain.ToString();
        int difficulty = GameController.currentDifficulty;

        /*Debug.Log("TOD: " + TOD);
        Debug.Log("season: " + season);
        Debug.Log("habitat: " + habitat);*/

        string vocalizationLoc = "tree";

        if(habitat == "BASIC")
        {
            //this should be a randomized choice bet. tree & bush, but as a start we'll just use tree
            vocalizationLoc = "tree";
        }
        else if(habitat == "WATER")
        {
            //do something else
        }

        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = "SELECT clipName " + "FROM sounds, birds WHERE TimeOfDay LIKE \"%" 
            + TOD + "%\" AND TimeOfYear LIKE \"%" + season 
            + "%\" AND habitat LIKE \"%" + vocalizationLoc + "%\""
            + "AND birds.difficulty=" + difficulty + " AND sounds.difficulty=" + difficulty + " AND birdName=name;";
        //Debug.Log(sqlQuery);
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();

        List<AudioClip> AudioClips = new List<AudioClip>();
        while (reader.Read())
        {
            //clipNames.Add(reader.GetString(0));
            foreach(AudioClip audio in AudioFiles)
            {
                if(audio.name == reader.GetString(0))
                {
                    AudioClips.Add(audio);
                }
            }

            //Debug.Log(reader.GetString(0));
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

        return AudioClips;
    }

    /// <summary>
    /// Given a clipName, the DB is queried for bird's image. 
    /// </summary>
    /// <param name="clipName"></param>
    /// <returns>The image file associated with the clip name.</returns>
    public Sprite GetImage(string clipName)
    {
        string[] queryResults = new string[2];
        Sprite found = null;
        dbconn = (IDbConnection)new SqliteConnection(conn);
        dbconn.Open(); //Open connection to the database
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = "SELECT imageFile, name FROM sounds, birds " +
                          "WHERE clipName=\"" + clipName + "\" AND birdName=name;";
        //Debug.Log(sqlQuery);
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        //Debug.Log(reader.FieldCount);
        while (reader.Read())
        {
            queryResults[0] = reader.GetString(0);
            queryResults[1] = reader.GetString(1);
        }
        currentBirdName = queryResults[1];
        foreach (Sprite image in ImageFiles)
        {
            if (image.name == queryResults[0])
            {
                found = image;
                break;
            }
        }

        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

        return found;
    }

    public List<List<string>> GetBirdNames(string excludedName)
    {
        
        List<List<string>> finalList = new List<List<string>>();

        dbconn = (IDbConnection)new SqliteConnection(conn);
        dbconn.Open(); //Open connection to the database
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = "SELECT name, imageFile FROM birds " 
                            + "WHERE name != "
                            + "\"" + excludedName + "\";";
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            List<string> nameANDimage = new List<string>();
            nameANDimage.Add(reader.GetString(0));
            nameANDimage.Add(reader.GetString(1));
            finalList.Add(nameANDimage);
        }

        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

        return finalList;
    }

    /// <summary>
    /// Returns image given its name.
    /// </summary>
    /// <param name="imageFileName">File name of image file.</param>
    /// <returns>Image file corresponding to name.</returns>
    public Sprite GetImageFromName(string imageFileName)
    {
        foreach(Sprite s in ImageFiles)
        {
            if(s.name == imageFileName)
            {
                return s;
            }
        }
        return null;
    }
}
