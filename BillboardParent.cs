﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardParent : MonoBehaviour {

    private Camera cameraToLookAt;
    private List<Transform> billboards;
    private int iTree = 0, iMod = 0;
    [SerializeField] private int K_MOD_FRAMES = 4;
    [SerializeField]
    private float fixedXRot, fixedZRot;

    void Start()
    {
        cameraToLookAt = Camera.main;
        billboards = new List<Transform>();
        foreach(Transform t in GetComponentsInChildren<Transform>())
        {
            if(t.gameObject != gameObject)
                billboards.Add(t);
        }
    }

    // Update is called once per frame
    void Update () {
        iMod = ++iMod % K_MOD_FRAMES;
        for (iTree = iMod; iTree < billboards.Count; iTree += K_MOD_FRAMES)
        {
            billboards[iTree].rotation = Quaternion.LookRotation(billboards[iTree].position -
                cameraToLookAt.transform.position); //rotates the billboard
                                                    //locks rotation on x-z-axes
            billboards[iTree].rotation = Quaternion.Euler(fixedXRot,
                                                  billboards[iTree].rotation.eulerAngles.y,
                                                  fixedZRot);
        }
    }
}
