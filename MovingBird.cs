﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

/// <summary>
/// Controls the behaviour of the moving bird.
/// </summary>
public class MovingBird : MonoBehaviour {

    public Transform player; 
    public float width; /*!< Determines x dimension of circle.*/
    public float height; /*!< Determines z dimension of circle.*/
    public float speed; /*!< Determines movement speed.*/
    //public float amplitude; 
    //public float duration;
    public int laps;   /*!< How many laps the bird has made around the player */
    public List<GameObject> treesInVicinity; /*!< Contains viable trees for bird to "land" on.*/
    //public GameObject dustContainer;
    public GameObject infoboxTemplate;
    public int numOfLaps; /*!< Number of laps the bird will make before initiating land sequence.*/
    public GameObject landingTree; /*!< Tree that the bird has landed on.*/
    public float infoboxAltitude; /*!< How high the info box will be placed.*/
    public GameObject LapCounter; /*!< Current number of laps made.*/

    private float timeCounter;
    private float oscillationProgress;
    //private bool creastReached; /*!< Has creast been reached?*/
    //private bool troughReached; /*!< Has trough been reached?*/
    private Vector3 orig; /*!< Bird's original position.*/
    //private IEnumerator currentBlend;    
    private bool stationary; /*!< Is bird stationary?*/

    // Use this for initialization
    void Start () {
        stationary = false;
        //treesInVicinity = new List<GameObject>(); 
        timeCounter = 0;
        laps = 0;
        orig = transform.position;
        oscillationProgress = orig.y;
        /*creastReached = false;
        troughReached = false;
        currentBlend = Oscillate(orig.y + amplitude, duration); //Start by moving upwards
        StartCoroutine(currentBlend);*/
        //width = 5;
        //height = 5;
        //speed = 1;
    }
	
	// Update is called once per frame
	/*void Update () {
        if (creastReached)
        {
            if(currentBlend != null)
            {
                StopCoroutine(currentBlend);
            }
            currentBlend = Oscillate(origY - amplitude, duration);
            StartCoroutine(currentBlend);
        }
        else if (troughReached)
        {
            if(currentBlend != null)
            {
                StopCoroutine(currentBlend);
            }
            currentBlend = Oscillate(origY + amplitude, duration);
            StopCoroutine(currentBlend);
        }
        
    }*/

    /// <summary>
    /// Not being used currently. Adds up and down movement to the bird.
    /// </summary>
    /// <param name="targetWeight">Target location in lerp</param>
    /// <param name="duration"></param>
    /// <returns></returns>
    IEnumerator Oscillate(float targetWeight, float duration)
    {
        float elapsedTime = 0f;
        float startProgress = oscillationProgress;
        while (elapsedTime < duration)
        {
            oscillationProgress = Mathf.Lerp(startProgress, targetWeight, elapsedTime);
            elapsedTime += Time.deltaTime;            
            yield return null;
        }
        oscillationProgress = targetWeight;
        /*if(oscillationProgress < orig.y)
        {
            troughReached = true;
        }   
        else if (oscillationProgress > orig.y)
        {
            creastReached = true;
        }*/   
    }

    /// <summary>
    /// Chooses a tree to land on and turns that tree into an
    /// interactable tree with infobox, sound, etc.
    /// </summary>
    /// <returns>Tree that has been chosen.</returns>
    GameObject InitiateLandingSeq()
    {
        float minDist = Single.PositiveInfinity;

        foreach (GameObject tree in treesInVicinity)
        {
            float dist = Vector3.Distance(tree.transform.position, transform.position);
            if(dist < minDist) 
            {
                minDist = dist;
                landingTree = tree;
            }
        }

        if (landingTree != null)
        {
            transform.parent = landingTree.transform;
            /*transform.localPosition = new Vector3(0,
                                    landingTree.GetComponent<Renderer>().bounds.size.y,
                                                  0);*/
            transform.localPosition = new Vector3(0,
                                    landingTree.GetComponentInChildren<Renderer>().bounds.size.y,
                                                  0);
            /*IntroInterfaceController.instance.dustContainer.transform.parent =
                                                            landingTree.transform;
            IntroInterfaceController.instance.dustContainer.transform.localPosition =
                                                            new Vector3(0, 3f, 0);*/        
            VRInteractiveItem VRInteracIt = landingTree.AddComponent<VRInteractiveItem>();
            IntroVRInteractiveItemApplied VRInteracItApp = landingTree.AddComponent<IntroVRInteractiveItemApplied>();
            VRInteracItApp.interactiveItem = VRInteracIt;
            infoboxTemplate.transform.parent = landingTree.transform;
            infoboxTemplate.transform.localPosition = new Vector3(0, infoboxAltitude, 0);
            stationary = true;
            GameController.instance.narrative = GameController.NarrativeState.LANDED;
            return landingTree;
        }
        return null;
    }

    /// <summary>
    /// Moves the bird in a cicular motion for a certain number of laps
    /// then initiates landing sequence to land the bird on a tree.
    /// </summary>
    /// <returns></returns>
    public GameObject CircleThenLand()
    {
        if (!stationary)
        {
            timeCounter += Time.deltaTime * speed;

            float x = Mathf.Cos(timeCounter) * width;
            float y = oscillationProgress;
            float z = Mathf.Sin(timeCounter) * height;

            transform.position = new Vector3(x, y, z) + player.position;

            //Counting laps
            laps += (transform.position.x == orig.x && transform.position.z == orig.z) ? 1 : 0;

            if (laps > numOfLaps)
            {
                LapCounter.SetActive(false);
                GetComponent<SpriteRenderer>().enabled = false;
                return InitiateLandingSeq();
            }
            //Debug.Log(laps);
        }
        return null;
    }
}
