﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The intro scene's audio controller.
/// </summary>
public class IntroAudioController : MonoBehaviour
{
    public static IntroAudioController instance;
    public GameObject focusedObject;
    public bool isFocused = false;

    private GameObject infoboxTemplate;

    void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        ControlSoundPlay();
    }

    /// <summary>
    /// Regulates what audio sources are allowed to emit sound at any given moment.
    /// </summary>
    void ControlSoundPlay()
    {
        if (isFocused)
        {            
            if (focusedObject != null)
            {
                //AudioSource focusedSource = focusedObject.transform.GetChild(0).gameObject.GetComponent<AudioSource>();
                AudioSource focusedSource = new AudioSource();
                foreach (Transform t in focusedObject.transform)
                {
                    if (t.gameObject.tag == "Circling Bird")
                    {
                        focusedSource = t.GetComponent<AudioSource>();
                        break;
                    }
                }
                //Finding infobox in tree with sound
                foreach (Transform t in focusedObject.transform)
                {
                    if(t.gameObject.tag == "Infobox")
                    {
                        //Finding VideoQuad in infobox
                        VideoInteractive vdInter = t.GetComponentInChildren<VideoInteractive>();
                        if(vdInter != null)
                        {
                            //If video isn't being played play audio, otherwise stop it
                            if (vdInter.allowOtherAudio)
                            {
                                if (!focusedSource.isPlaying)
                                {
                                    //Debug.Log("Playing focusedSource");
                                    focusedSource.Play();
                                }
                            }
                            else
                            {
                                focusedSource.Stop();
                            }
                        }
                    }
                }
                //This used to be here
                /*if (!focusedSource.isPlaying)
                {
                    //Debug.Log("Playing focusedSource");
                    focusedSource.Play();
                }*/                
            }            
        }
        else
        {
            if (focusedObject != null)
            {
                //AudioSource focusedSource = focusedObject.transform.GetChild(0).gameObject.GetComponent<AudioSource>();
                AudioSource focusedSource = new AudioSource();
                foreach (Transform t in focusedObject.transform)
                {
                    if (t.gameObject.tag == "Circling Bird")
                    {
                        focusedSource = t.GetComponent<AudioSource>();
                        break;
                    }
                }
                if (focusedSource.isPlaying)
                    focusedSource.Stop();
            }
        }
    }
}
