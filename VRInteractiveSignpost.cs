﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using VRStandardAssets.Utils;

/// <summary>
/// Controls the action that happens when a sign on the signpost
/// is clicked.
/// </summary>
public class VRInteractiveSignpost : MonoBehaviour {

    public bool isCorrectAnswer = false;

    private VRInteractiveItem interactiveItem;
    private string origText;
    private TextMeshProUGUI text;
    private AnswerManager answerManager;
    private AudioSource audioFeedback;
    [SerializeField] private AudioClip correctAnswer,
                                       wrongAnswer;
    [SerializeField] private SignpostDrop signpostDrop;
    private SpriteRenderer sr; 

    // Use this for initialization
    void Start () {
        audioFeedback = GetComponent<AudioSource>();
        interactiveItem = GetComponent<VRInteractiveItem>();
        text = GetComponentInChildren<TextMeshProUGUI>();
        answerManager = transform.parent.parent.parent.gameObject.GetComponent<AnswerManager>();
        if (text != null)
        {
            origText = text.text;
        }
        
        if (interactiveItem != null)
        {
            interactiveItem.OnOver += HandleOver;
            interactiveItem.OnOut += HandleOut;
            interactiveItem.OnClick += HandleClick;
        }
    }

    private void OnEnable()
    {
        if (interactiveItem != null)
        {
            interactiveItem.OnOver += HandleOver;
            interactiveItem.OnOut += HandleOut;
            interactiveItem.OnClick += HandleClick;
        }
    }

    private void OnDisable()
    {
        if (interactiveItem != null)
        {
            interactiveItem.OnOver -= HandleOver;
            interactiveItem.OnOut -= HandleOut;
            interactiveItem.OnClick -= HandleClick;
        }
    }

    private void HandleOver()
    {
        if(text != null)
        {
            text.text = "<b><u>" + origText + "</u></b>"; 
        }
    }

    private void HandleOut()
    {
        if (text != null)
        {
            text.text = origText;
        }
    }

    private void HandleClick()
    {
        if(answerManager != null)
        {
            if (!isCorrectAnswer) //If chosen answer is wrong
            {
                //give visual/acoustic feedback that answer is wrong.  
                audioFeedback.clip = wrongAnswer;
                audioFeedback.Play();
                answerManager.answered = false; //May be redundant, just reinforcing
            }
            else //If chosen answer is right
            {
                audioFeedback.clip = correctAnswer;
                audioFeedback.Play();
                answerManager.answered = true;
                //make flying bird appear
                sr = answerManager.GetComponentInChildren<SpriteRenderer>(true);
                if (sr != null)
                    sr.gameObject.SetActive(true);
                signpostDrop.RemoveStarter(3f, sr);
                //answerManager.ActivateInfobox();                
            }
        }
    }
}
