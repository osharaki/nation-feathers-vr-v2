﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using VRStandardAssets.Utils;

/// <summary>
/// Manages all user interface in the game.
/// </summary>
public class InterfaceController : MonoBehaviour {

    public static InterfaceController instance;
    public GameObject infoboxTemplate, /*!< Info box template to be copied and used on all trees*/
                      lookUPPrompt,
                      dustContainerTemplate,
                      signpostTemplate,
                      player,
                      GUIReticle,
                      EnterGatePrompt,
                      PressBackButtonPrompt;
    public Camera myCamera;
    public float infoboxAltitude; /*!< How high the info box will be set.*/
    public bool checkDist = true;

    private float roomToMove = 2f;

    GameObject target; /*!<Gets updated with AudioController's current focusedObject (i.e. tree, bush, etc.) whenever AudioController's isFocused value is true.*/
    GameObject[] infoboxPool; /*!< Object pool containing info boxes that are instantiated in Start.*/
    GameObject[] dustContainerPool; /*!< Object pool containing dust effect objects.*/
    GameObject[] signpostPool; /*!< Object pool containing signpost objects.*/
    List<GameObject> activeInfoboxes; /*!< List containing all info boxes that have been assigned to trees.*/
    List<GameObject> activeDustContainers; /*!< List containing all dust effects that have been assigned to trees.*/
    List<GameObject> activeSignposts; /*!< List containing all signposts that have been assigned to trees.*/
    GameObject infobox; /*!< Contains info box that is currently being added.*/
    GameObject dustContainer; /*!< Contains dust container that is currently being added.*/
    GameObject signpost; /*!< Contains signpost that is currently being added.*/
    Text textbox; /*!< Info box's textbox.*/
    Image image, /*!< Info box's image.*/
          image_generic; /*!< Generic image in challenge mode's Info box.*/
    VideoPlayer vp; /*!< Info box's video player.*/    

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start ()
    {
        activeInfoboxes = new List<GameObject>();
        activeDustContainers = new List<GameObject>();
        activeSignposts = new List<GameObject>();
        infoboxPool = new GameObject[GameController.instance.sizeOfPools];
        dustContainerPool = new GameObject[GameController.instance.sizeOfPools];
        signpostPool = new GameObject[GameController.instance.sizeOfPools];

        //Initializing infoboxPool with infoboxes
        /*for (int i = 0; i < infoboxPool.Length; i++)
        {
            infoboxPool[i] = Instantiate(infoboxTemplate);
            //Activate infoboxes and their videos to prepare() them
            //then deactivate them
            infoboxPool[i].SetActive(true);
            VideoInteractive vdInter = infoboxPool[i].GetComponentInChildren<VideoInteractive>(true);
            vdInter.gameObject.SetActive(true);
            GameObject[] GOArray = new GameObject[2];
            GOArray[0] = vdInter.gameObject;
            GOArray[1] = infoboxPool[i];
            StartCoroutine(DisableAfterPrepare(vdInter.videoPlayer, GOArray));
            //infoboxPool[i].SetActive(false);
        }*/
        for (int i = 0; i < infoboxPool.Length; i++)
        {
            infoboxPool[i] = Instantiate(infoboxTemplate);
            infoboxPool[i].SetActive(false);
        }
        //Initializing infoboxPool with infoboxes
        //StartCoroutine("VideoPreparer");

        //Initializing dustContainerPool with dust
        /*for (int i = 0; i < dustContainerPool.Length; i++)
        {
            dustContainerPool[i] = Instantiate(dustContainerTemplate);
            dustContainerPool[i].SetActive(false);
        }*/

        if(GameController.instance.sceneName == "Challenge Forest Summer")
        {
            //Initializing signpostPool with signposts
            for (int i = 0; i < signpostPool.Length; i++)
            {
                signpostPool[i] = Instantiate(signpostTemplate);
                signpostPool[i].SetActive(false);
            }
        }        
    }

    void Update()
    {
        if(GameController.instance.sceneName == "Basic Forest Summer" && checkDist)
            CheckDistance();
        if (GameController.instance.sceneName == "Challenge Forest Summer" && checkDist)
            CheckDistanceChallenge();
    }

    /// <summary>
    /// Initializes infobox pool and enables videoplayer to initiate
    /// video preparation then waits for videos to be prepared before
    /// disabling first the videos followed by the infoboxes themselves
    /// thus returning them to thei original states.
    /// </summary>
    /// <returns></returns>
    IEnumerator VideoPreparer()
    {
        //Initializing infoboxPool with infoboxes
        for (int i = 0; i < infoboxPool.Length; i++)
        {
            //infoboxPool[i] = Instantiate(infoboxTemplate);
            //Activate infoboxes and their videos to prepare() them
            //then deactivate them
            infoboxPool[i].SetActive(true);
            VideoInteractive vdInter = infoboxPool[i].GetComponentInChildren<VideoInteractive>(true);
            vdInter.gameObject.SetActive(true);
            while (!vdInter.videoPlayer.isPrepared)
            {
                yield return null;
            }
            vdInter.gameObject.SetActive(false);
            infoboxPool[i].SetActive(false);
            //GameObject[] GOArray = new GameObject[2];
            //GOArray[0] = vdInter.gameObject;
            //GOArray[1] = infoboxPool[i];
            //StartCoroutine(DisableAfterPrepare(vdInter.videoPlayer, GOArray));
            //infoboxPool[i].SetActive(false);
        }
        AudioController.instance.EnableAddNewSound();
    }

    /// <summary>
    /// Waits for video to prepare then disables parameters (Not being used)
    /// </summary>
    /// <param name=""></param>
    /// <returns></returns>
    IEnumerator DisableAfterPrepare(VideoPlayer vp, GameObject[] GOArray)
    {
        while (!vp.isPrepared)
        {
            yield return null;
        }
        foreach(GameObject GO in GOArray)
        {
            GO.SetActive(false);
        }
    }

    /// <summary>
    /// Retrieves an infobox from the infobox pool.
    /// </summary>
    /// <returns>Info box game object retrieved from pool.</returns>
    private GameObject GetInfobox()
    {
        for (int i = 0; i < infoboxPool.Length; i++)
        {
            if (!infoboxPool[i].activeSelf)
            {
                infoboxPool[i].SetActive(true);
                return infoboxPool[i];
            }
        }
        return null;
    }

    /// <summary>
    /// Retrieves a dustContainer object from dustContainer pool.
    /// </summary>
    /// <returns></returns>
    GameObject GetDust()
    {
        for (int i = 0; i < dustContainerPool.Length; i++)
        {
            if (!dustContainerPool[i].activeSelf)
            {
                dustContainerPool[i].SetActive(true);
                return dustContainerPool[i];
            }
        }
        return null;
    }

    GameObject GetSignpost()
    {
        for (int i = 0; i < signpostPool.Length; i++)
        {
            if (!signpostPool[i].activeSelf)
            {
                signpostPool[i].SetActive(true);
                return signpostPool[i];
            }
        }
        return null;
    }

    /// <summary>
    /// Manages how infoboxes behave when their parernt objects are clicked. 
    /// Called by HandleClick in VRInteractiveItemApplied. HandleClick is subscribed
    /// to OnClick.
    /// </summary>
    public void ManageInfobox()
    {        
        Text aTextbox;
        Image anImage = null;
        GameObject videoQuad;
        foreach (GameObject localInfobox in activeInfoboxes)
        {
            //this if statement might be redundant now that we're sort of already checking this
            //in VRInteractiveItemApplied's HandleClick function.
            if(localInfobox.transform.parent.gameObject == AudioController.instance.focusedObject)
            {
                aTextbox = localInfobox.GetComponentInChildren<Text>(true);
                //In order to differentiate between generic image and normal image in Challenge scene's infobox.
                foreach (Image i in localInfobox.GetComponentsInChildren<Image>(true))
                {
                    if(i.gameObject.tag != "Infobox_Image_Generic")
                    {
                        anImage = i;
                    }
                }
                videoQuad = localInfobox.GetComponentInChildren<VideoPlayer>(true).gameObject.transform.parent.gameObject;                
                
                //this if statement probably never gets fulfilled, consider removing 
                if (AudioController.instance.isFocused && localInfobox != null
                                                        && !localInfobox.activeSelf) 
                {
                    if(anImage != null)
                    {
                        anImage.gameObject.SetActive(true);
                    }
                    break;
                }
                else if (AudioController.instance.isFocused && localInfobox != null)
                {
                    if (aTextbox != null)
                    {
                        aTextbox.gameObject.SetActive(true);
                    }
                    if (videoQuad != null)
                    {
                        videoQuad.SetActive(true);
                    }
                    else
                    {
                        Debug.Log("VideoQuad not found!");
                    }
                    if (anImage != null && !anImage.gameObject.activeSelf)
                    {
                        anImage.gameObject.SetActive(true);
                    }
                    //ActivateLookUpPrompt(anImage);
                    break;
                }
                else if (!AudioController.instance.isFocused)
                {
                    if (aTextbox != null)
                    {
                        aTextbox.gameObject.SetActive(false);
                    }
                    if (videoQuad != null)
                    {
                        videoQuad.SetActive(false);
                    }
                    break;
                }
            }            
        }        
    }

    /// <summary>
    /// Manages how infoboxes behave when their parernt objects are clicked. 
    /// Called by HandleClick in ChallengeVRInteractiveItemApplied. 
    /// HandleClick is subscribed to OnClick.
    /// </summary>
    public void ManageInfoboxChallenge()
    {
        Text aTextbox;
        Image anImage = null;
        GameObject videoQuad;
        foreach (GameObject localInfobox in activeInfoboxes)
        {
            //this if statement might be redundant now that we're sort of already checking this
            //in VRInteractiveItemApplied's HandleClick function.
            if (localInfobox.transform.parent.gameObject == ChallengeAudioController.instance.focusedObject)
            {
                aTextbox = localInfobox.GetComponentInChildren<Text>(true);
                //In order to differentiate between generic image and normal image in Challenge scene's infobox.
                foreach (Image i in localInfobox.GetComponentsInChildren<Image>(true))
                {
                    if (i.gameObject.tag != "Infobox_Image_Generic")
                    {
                        anImage = i;
                    }
                }
                videoQuad = localInfobox.GetComponentInChildren<VideoPlayer>(true).gameObject.transform.parent.gameObject;

                //this if statement probably never gets fulfilled, consider removing 
                if (ChallengeAudioController.instance.isFocused && localInfobox != null
                                                        && !localInfobox.activeSelf)
                {
                    if (anImage != null)
                    {
                        anImage.gameObject.SetActive(true);
                    }
                    break;
                }
                else if (ChallengeAudioController.instance.isFocused && localInfobox != null)
                {
                    if (aTextbox != null)
                    {
                        aTextbox.gameObject.SetActive(true);
                    }
                    if (videoQuad != null)
                    {
                        videoQuad.SetActive(true);
                    }
                    else
                    {
                        Debug.Log("VideoQuad not found!");
                    }
                    if (anImage != null && !anImage.gameObject.activeSelf)
                    {
                        anImage.gameObject.SetActive(true);
                    }
                    //ActivateLookUpPrompt(anImage);
                    break;
                }
                else if (!ChallengeAudioController.instance.isFocused)
                {
                    if (aTextbox != null)
                    {
                        aTextbox.gameObject.SetActive(false);
                    }
                    if (videoQuad != null)
                    {
                        videoQuad.SetActive(false);
                    }
                    break;
                }
            }
        }
    }
    public void ActivateInfoboxGenericImage(GameObject genericImage)
    {
        if (genericImage != null)
            genericImage.SetActive(true);
    }

    public void DeactivateInfoboxGenericImage(GameObject genericImage)
    {
        if (genericImage != null)
            genericImage.SetActive(false);
    }

    public void ActivateInfoboxImage(GameObject image)
    {
        if (image != null)
            image.SetActive(true);
    }

    public void DeactivateInfoboxImage(GameObject image)
    {
        if (image != null)
            image.SetActive(false);
    }

    public void ActivateInfoboxText(GameObject text)
    {
        if (text != null)
            text.SetActive(true);
    }

    public void DeactivateInfoboxText(GameObject text)
    {
        if (text != null)
            text.SetActive(false);
    }

    public void ActivateInfoboxVideo(GameObject video)
    {
        if (video != null)
            video.SetActive(true);
    }

    public void DeactivateInfoboxVideo(GameObject video)
    {
        if (video != null)
            video.SetActive(false);
    }
    /*public void ManageSignpost(GameObject localSignpost)
    {

        foreach(Transform signpostChild in localSignpost.transform)
        {
            signpostChild.gameObject.SetActive(true);
        }
    }*/

    /// <summary>
    /// Adds an infobox. Called by AddInteractibility() in AudioManager.
    /// </summary>
    /// <param name="target">Object which will will be the parent of the infobox.</param>
    public void AddInfobox(GameObject target)
    {
        infobox = GetInfobox();
        if (infobox != null)
        {
            activeInfoboxes.Add(infobox);

            //Get data from DB                
            textbox = infobox.GetComponentInChildren<Text>(true);
            image = infobox.GetComponentInChildren<Image>(true);
            image.sprite = DBController.instance.GetImage(AudioController.instance.targetSource.clip.name);
            textbox.text = DBController.instance.currentBirdName;
            //Add video clip
            VideoPlayer vp = infobox.GetComponentInChildren<VideoPlayer>(true);
            if (vp != null)
            {
                vp.clip = DBController.instance.VideoFiles[AudioController.instance.Index];
            }
            //Place infobox at right position
            infobox.transform.parent = target.transform;
            infobox.transform.localPosition = new Vector3(0, infoboxAltitude, 0);
            foreach(Transform t in DBController.instance.transform)
            {
                if(t.tag == image.sprite.name)
                {
                    t.parent = infobox.transform;
                    t.localPosition = new Vector3(-1.846007f,
                                                  -5.093333f,
                                                  -3.013257f);
                    t.rotation = Quaternion.Euler(0, 0, 0);
                    t.GetComponent<CameraFacingBillboard>().ObjectToLookAt = t.parent.gameObject;
                    t.GetComponent<CirclingBird>().pivot = t.parent.gameObject;
                }
            }
        }
        else
        {
            Debug.Log("infoboxPool contains no more infoboxes");
        }
    }

    public void AddInfoboxChallenge(GameObject target)
    {
        infobox = GetInfobox();
        if (infobox != null)
        {
            activeInfoboxes.Add(infobox);

            //Get data from DB                
            textbox = infobox.GetComponentInChildren<Text>(true);
            foreach(Image i in infobox.GetComponentsInChildren<Image>(true))
            {
                if (i.gameObject.tag == "Infobox_Image")
                    image = i;
                if (i.gameObject.tag == "Infobox_Image_Generic")
                    image_generic = i;
            }
            
            //image = infobox.GetComponentInChildren<Image>(true);
            image.sprite = DBController.instance.GetImage(ChallengeAudioController.instance.targetSource.clip.name);
            textbox.text = DBController.instance.currentBirdName;
            //Add video clip
            VideoPlayer vp = infobox.GetComponentInChildren<VideoPlayer>(true);
            if (vp != null)
            {
                vp.clip = DBController.instance.VideoFiles[ChallengeAudioController.instance.Index];
            }
            //Place infobox at right position
            infobox.transform.parent = target.transform;
            infobox.transform.localPosition = new Vector3(0, infoboxAltitude, 0);
            foreach (Transform t in DBController.instance.transform)
            {
                if (t.tag == image.sprite.name)
                {
                    t.parent = infobox.transform;
                    t.localPosition = new Vector3(-1.846007f,
                                                  -5.093333f,
                                                  -3.013257f);
                    t.rotation = Quaternion.Euler(0, 0, 0);
                    t.GetComponent<CameraFacingBillboard>().ObjectToLookAt = t.parent.gameObject;
                    t.GetComponent<CirclingBird>().pivot = t.parent.gameObject;
                }
            }
        }
        else
        {
            Debug.Log("infoboxPool contains no more infoboxes");
        }
    }

    /// <summary>
    /// Adds a dustContainer. Called by AddInteractibility() in AudioManager.
    /// </summary>
    /// <param name="target">Object which will be the parent of the dustContainer.</param>
    public void AddDustEffect(GameObject target)
    {
        dustContainer = GetDust();
        if(dustContainer != null)
        {
            activeDustContainers.Add(dustContainer);

            //Place dustContainer at right position
            dustContainer.transform.parent = target.transform;
            dustContainer.transform.localPosition = new Vector3(0, 8f, 0);
        }
        else
        {
            Debug.Log("dustContainerPool contains no more dustContainers");
        }
    }

    /// <summary>
    /// Adds a signpost .
    /// </summary>
    /// <param name="target">The object that will be the parent of the 
    /// signpost, e.g. a tree.</param>
    public void AddSignpost(GameObject target)
    {
        signpost = GetSignpost();
        if (signpost != null)
        {
            activeSignposts.Add(signpost);

            //Place dustContainer at right position
            signpost.transform.parent = target.transform;
            signpost.transform.localPosition = new Vector3(0, 0, 0);
        }
        else
        {
            Debug.Log("signpostPool contains no more signposts");
        }
    }

    public void ActivateDust(GameObject dustContainer)
    {
        dustContainer.transform.GetChild(0).gameObject.SetActive(true); //Activate dust effect.
    }

    public void DeactivateDust(GameObject dustContainer)
    {
        dustContainer.transform.GetChild(0).gameObject.SetActive(false); //Activate dust effect.
    }

    public GameObject GetLocalDustContainer(GameObject inquirer)
    {
        foreach(GameObject localDustContainer in activeDustContainers)
        {
            if(localDustContainer.transform.parent.gameObject ==
                inquirer.gameObject)
            {
                return localDustContainer;
            }
        }
        return null;
    }

    /// <summary>
    /// Checks to see if player is far enough from a focusedObject.
    /// If player is a certain distance away from focusedObject then the
    /// focusedObject is unfocused. More specifically, an unfocusing click is simulated.
    /// </summary>
    void CheckDistance()
    {
        if (AudioController.instance.isFocused)
        {
            if (AudioController.instance.focusedObject != null)
            {
                /*if (Vector3.Distance(player.transform.position,
                    AudioController.instance.focusedObject.transform.position) >
                                    myCamera.GetComponent<VREyeRaycaster>().m_RayLength + 5)
                {
                    //Simulate an unfocusing click
                    AudioController.instance.focusedObject.GetComponent<VRInteractiveItemApplied>().HandleClick();
                }*/
                if ((Vector2.Distance(new Vector2(player.transform.position.x, player.transform.position.z)
                    , new Vector2(AudioController.instance.focusedObject.transform.position.x,
                                  AudioController.instance.focusedObject.transform.position.z)) -
                                  AudioController.instance.focusedObject.GetComponent<CapsuleCollider>().radius)
                                         > myCamera.GetComponent<VREyeRaycaster>().m_RayLength + roomToMove)
                {
                    AudioController.instance.focusedObject.GetComponent<VRInteractiveItemApplied>().HandleClick();
                }
            }
        }        
    }

    void CheckDistanceChallenge()
    {
        if (ChallengeAudioController.instance.isFocused)
        {
            if (ChallengeAudioController.instance.focusedObject != null)
            {
                /*if (Vector3.Distance(player.transform.position,
                    ChallengeAudioController.instance.focusedObject.transform.position) >
                                    myCamera.GetComponent<VREyeRaycaster>().m_RayLength + 5)
                {
                    //Simulate an unfocusing click
                    ChallengeAudioController.instance.focusedObject.GetComponent<ChallengeVRInteractiveItemApplied>().HandleClick();
                }*/
                if ((Vector2.Distance(new Vector2(player.transform.position.x, player.transform.position.z)
                    , new Vector2(ChallengeAudioController.instance.focusedObject.transform.position.x,
                                  ChallengeAudioController.instance.focusedObject.transform.position.z)) -
                                  ChallengeAudioController.instance.focusedObject.GetComponent<CapsuleCollider>().radius)
                                         > myCamera.GetComponent<VREyeRaycaster>().m_RayLength + roomToMove)
                {
                    ChallengeAudioController.instance.focusedObject.GetComponent<ChallengeVRInteractiveItemApplied>().HandleClick();
                }
            }
        }
    }

    /// <summary>
    /// Called on first activation of an infobox. 
    /// Displays a prompt to the player to look up if infobox
    /// is out of camera view when player clicked a tree.
    /// Disables reticle while it's active to prevent focus distortion.
    /// </summary>
    void ActivateLookUpPrompt(Image image)
    {        
        Vector3 check = myCamera.WorldToViewportPoint(image.transform.position);
        //Debug.Log(check);
        if (!((check.x >= 0 && check.x <= 1) &&
            (check.y >= 0 && check.y <= 1) &&
            (check.z >= 0)))
        {
            //Debug.Log("Target not in camera view. Add look up.");
            //Target not in camera view. Add look up.
            //Look for UpIcon in hierarchy.
            if(lookUPPrompt != null)
                lookUPPrompt.gameObject.SetActive(true);
            GUIReticle.SetActive(false);
            StartCoroutine("DeactivateLookUpPrompt", image);
        }        
    }

    /// <summary>
    /// Deactivates the prompt created by ActivateLookUpPrompt
    /// when infobox appears in camera view as player moves his head.
    /// Reenables reticle.
    /// </summary>
    IEnumerator DeactivateLookUpPrompt(Image image)
    {        
        Vector3 check = myCamera.WorldToViewportPoint(image.transform.position);
        //Debug.Log(check);
        while (!((check.x >= 0 && check.x <= 1) &&
            (check.y >= 0 && check.y <= 1) &&
            (check.z >= 0)))
        {
            //Debug.Log("Target still not in camera view.");
            check = myCamera.WorldToViewportPoint(image.transform.position);
            //Target not in camera view. Add look up.
            yield return null;
        }
        //Debug.Log("Target in camera view.");
        //Look for UpIcon in hierarchy
        if (lookUPPrompt != null)
            lookUPPrompt.gameObject.SetActive(false);
        GUIReticle.SetActive(true);
    }

    public void ActivateEnterGatePrompt()
    {
        EnterGatePrompt.SetActive(true);
        StartCoroutine("DeactivateEnterGatePrompt");
    }

    public void ActivatePressBackButton()
    {
        PressBackButtonPrompt.SetActive(true);
        StartCoroutine("DeactivatePressBackButtonPrompt");
    }

    IEnumerator DeactivateEnterGatePrompt()
    {
        while(EnterGatePrompt.GetComponentInChildren<Text>().color.a != 0)
        {
            yield return null;
        }
        EnterGatePrompt.SetActive(false);
    }

    IEnumerator DeactivatePressBackButtonPrompt()
    {
        while (PressBackButtonPrompt.GetComponentInChildren<Text>().color.a != 0)
        {
            yield return null;
        }
        PressBackButtonPrompt.SetActive(false);
    }
}
