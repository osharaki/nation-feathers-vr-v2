﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using VRStandardAssets.Utils;

/// <summary>
/// Controls video playback on click.
/// </summary>
public class OnDemandPlay : MonoBehaviour {    

    public VideoPlayer videoPlayer;
    public AudioSource audioSource;
    public VRInteractiveItem interactiveItem;
    public RawImage image;

    private float elapsed;
    private bool startCount;
    private Texture thumb, videoTexture;

    // Use this for initialization
    void Start()
    {
        //thumb = image.texture;
        elapsed = 0;
        startCount = false;
        interactiveItem.OnClick += HandleClick;

        StartCoroutine("playVideo");
    }

    void Update()
    {
        if (startCount)
        {
            elapsed += Time.deltaTime;
            //Eventhough video seems to stop it doesn't always register as such
            //and VideoPlayer.isPlaying continues to return true. This makes sure 
            //that after a duration of time equal to the length of the video has passed 
            //the video is manually stopped. VideoPlayer.Time is not being used
            //because playback can freeze and VideoPlayer.Time will not move forward. 
            if (elapsed >= videoPlayer.clip.length && videoPlayer.isPlaying)
            {
                videoPlayer.Stop();
                startCount = false;
                //image.texture = thumb;
                Debug.Log("Video stopped.");
            }
        }
    }

    IEnumerator playVideo()
    {
        //Add VideoPlayer to the GameObject
        //videoPlayer = gameObject.AddComponent<VideoPlayer>();

        //Add AudioSource
        //audioSource = gameObject.AddComponent<AudioSource>();

        //Disable Play on Awake for both Video and Audio
        videoPlayer.playOnAwake = false;
        audioSource.playOnAwake = false;
        audioSource.Pause();
        //videoPlayer.isLooping = true;

        //We want to play from video clip not from url

        //videoPlayer.source = VideoSource.VideoClip;

        // Vide clip from Url
        //videoPlayer.source = VideoSource.Url;
        //videoPlayer.url = "http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";


        //Set Audio Output to AudioSource
        //videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        videoPlayer.EnableAudioTrack(0, true);
        //videoPlayer.SetTargetAudioSource(0, audioSource);

        //Set video To Play then prepare Audio to prevent Buffering
        //videoPlayer.clip = videoToPlay;
        videoPlayer.Prepare();

        //Wait until video is prepared
        WaitForSeconds waitTime = new WaitForSeconds(1);
        while (!videoPlayer.isPrepared)
        {
            Debug.Log("Preparing Video");
            //Prepare/Wait for 5 sceonds only
            yield return waitTime;
            //Break out of the while loop after 5 seconds wait
            break;
        }

        Debug.Log("Done Preparing Video");
        
        //Assign the Texture from Video to RawImage to be displayed
        //image.texture = videoPlayer.texture;

        //Play Video
        //videoPlayer.Play();

        //Play Sound
        //audioSource.Play();

        //Debug.Log("Playing Video");
        /*while (videoPlayer.isPlaying)
        {
            Debug.LogWarning("Video Time: " + Mathf.FloorToInt((float)videoPlayer.time));
            //yield return null;
            yield return new WaitForSeconds((float)videoToPlay.length); //added by osharaki
            break;
        }*/
        /*while (true)
        {
            videoPlayer.Prepare();

            //Wait until video is prepared
            WaitForSeconds waitTime = new WaitForSeconds(1);
            while (!videoPlayer.isPrepared)
            {
                Debug.Log("Preparing Video");
                //Prepare/Wait for 5 sceonds only
                yield return waitTime;
                //Break out of the while loop after 5 seconds wait
                break;
            }
            Debug.Log("Done Preparing Video");
            image.texture = videoPlayer.texture;
            //Play Video
            videoPlayer.Play();
            Debug.Log("Video played!");
            //Play Sound
            audioSource.Play();
            while (videoPlayer.isPlaying)
            {
                Debug.LogWarning("Video Time: " + Mathf.FloorToInt((float)videoPlayer.time));
                yield return null;                
            }
            Debug.Log("Video ended.");
        }*/

        //Debug.Log("Done Playing Video");        
    }

    void HandleClick()
    {
        if (videoPlayer.isPlaying)
        {
            /*videoPlayer.Stop();
            audioSource.Stop();
            videoPlayer.Play();
            audioSource.Play();*/

            //Uncommenting the above segment allows for video
            //to be restarted before finishing.
        }
        else
        {
            Debug.Log("Clicked");
            //image.texture = videoTexture;
            //image.texture = videoPlayer.texture;
            videoPlayer.Play();
            audioSource.Play();
            elapsed = 0;
            startCount = true;
        }
    }
}
