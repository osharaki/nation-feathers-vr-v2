﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

/// <summary>
/// Handles the action performed on collision with the gateway quad.
/// </summary>
public class GatewayTrigger : MonoBehaviour {

    public GameObject myCamera;

    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Gateway trigger entered!");
        //GameController.instance.LoadNextScene();
        gameObject.SetActive(false);
        myCamera.GetComponent<VRCameraFade>().FadeOut(false);
    }
}
