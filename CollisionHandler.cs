﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionHandler : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        GameController.instance.narrative = GameController.NarrativeState.MOVED;
        gameObject.SetActive(false);
    }
}
