﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class Tile
{
    public GameObject theTile;
    public float creationTime;
    
    public Tile(GameObject t, float ct)
    {
        theTile = t;
        creationTime = ct;
    }
}
public class GenerateInfiniteFPV : MonoBehaviour {

    public static GenerateInfiniteFPV Instance;
    public GameObject plane;
    public GameObject player;
    public int halfTilesX = 10;
    public int halfTilesZ = 10;

    int planeSize = 10;    
    Vector3 startPos;
    Hashtable tiles = new Hashtable();

    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	void Start () {
        transform.position = Vector3.zero;
        startPos = Vector3.zero;

        float updateTime = Time.realtimeSinceStartup;
        
        for(int x = -halfTilesX; x <= halfTilesX; x++)
        {
            for(int z = -halfTilesZ; z <= halfTilesZ; z++)
            {
                Vector3 pos = new Vector3((x * planeSize + startPos.x),
                                           0,
                                           (z * planeSize + startPos.z));
                GameObject t = Instantiate(plane, pos, Quaternion.identity);                
                string tilename = "Tile_" + ((int)(pos.x)).ToString() + "_" + ((int)(pos.z)).ToString();
                t.name = tilename;
                Tile tile = new Tile(t, updateTime);
                tiles.Add(tilename, tile);
                if((Mathf.Abs(x) % halfTilesX == 0) && (Mathf.Abs(z) % halfTilesZ == 0))
                {
                    //Debug.Log(Mathf.Abs(x) + " % " + "10 = " + (Mathf.Abs(x) % 10));
                    t.GetComponent<GenerateTerrain>().addSound = true;
                }
            }
        }	
	}
}
