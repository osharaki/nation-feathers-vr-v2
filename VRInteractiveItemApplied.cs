﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

public class VRInteractiveItemApplied : MonoBehaviour {

    public VRInteractiveItem interactiveItem;

    private GameObject thisDustContainer;
    private Reticle r;
    private bool hasBeenActivated = false;
    private SpriteRenderer sr;

    private void Start()
    {
        r = Camera.main.GetComponent<Reticle>();
        if (interactiveItem != null)
        {
            interactiveItem.OnOver += HandleOver;
            interactiveItem.OnOut += HandleOut;
            interactiveItem.OnClick += HandleClick;
        }

        thisDustContainer = InterfaceController.instance.GetLocalDustContainer(this.gameObject);                
    }
    private void OnEnable()
    {
        if(interactiveItem != null)
        {
            interactiveItem.OnOver += HandleOver;
            interactiveItem.OnOut += HandleOut;
            interactiveItem.OnClick += HandleClick;
        }        
    }

    private void OnDisable()
    {
        if (interactiveItem != null)
        {
            interactiveItem.OnOver -= HandleOver;
            interactiveItem.OnOut -= HandleOut;
            interactiveItem.OnClick -= HandleClick;
        }
    }

    private void HandleOver()
    {
        //Debug.Log("Object is highlighted");
        if(thisDustContainer != null)
        {
            //InterfaceController.instance.ActivateDust(thisDustContainer);
            
            
        }
        
        if (!hasBeenActivated)
        {
            //r.HideDot();
            sr = GetComponentInChildren<SpriteRenderer>(true);
            if (sr != null)
                sr.gameObject.SetActive(true);
        }
        else if (r != null)
        {
            r.Show(); //Activate large reticle
        }
    }

    private void HandleOut()
    {
        if(sr == null)
            sr = GetComponentInChildren<SpriteRenderer>(true);
        if (sr != null)
            sr.gameObject.SetActive(false);
        //r.ShowDot();
        //Debug.Log("Object is NOT highlighted");
        if (thisDustContainer != null)
        {
            //InterfaceController.instance.DeactivateDust(thisDustContainer);
            
            
        }
        if (hasBeenActivated)
        {
            if (r != null)
            {
                r.Hide(); //Deactivate large reticle
            }
        }        
    }

    public void HandleClick()
    {
        hasBeenActivated = true;
        if (sr != null)
            sr.gameObject.SetActive(false);
        if (AudioController.instance.isFocused)
        {
            if(AudioController.instance.focusedObject == gameObject)
            {
                AudioController.instance.focusedObject = gameObject;
                AudioController.instance.isFocused = !AudioController.instance.isFocused;
                InterfaceController.instance.ManageInfobox();
                
                //InterfaceController.instance.AddDustEffect();
                //Debug.Log("Object has been clicked");
            }
        }
        else
        {
            AudioController.instance.focusedObject = gameObject;
            AudioController.instance.isFocused = !AudioController.instance.isFocused;
            InterfaceController.instance.ManageInfobox();
            if (gameObject == AudioController.instance.targetObject &&
                AudioController.instance.audioSourceList.Count == 0)
            {
                if (GameController.currentDifficulty < GameController.maxDifficulty)
                {
                    GameController.currentDifficulty++;
                    AudioController.instance.alreadyGotAudioList = false;
                }
                else if (!AudioController.instance.waveOver)
                {
                    AudioController.instance.waveOver = !AudioController.instance.waveOver;
                }
            }
            //InterfaceController.instance.AddDustEffect();
            //Debug.Log("Object has been clicked");
        }
    }
}
