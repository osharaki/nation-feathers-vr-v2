﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls the treePool which contains all trees in the game.
/// </summary>
public class TreePool : MonoBehaviour {

    public static TreePool Instance;
    public static int totalTrees = 30;    //total number of trees in the pool
    public GameObject[] treePrefabs;    /*!< This array contains the different tree objects to be chosen from randomly for the pool.*/
    public bool generateNewTrees = false;
    static GameObject[] trees;

    void Awake()    //Used awake to guarantee that this gets initialized before GenerateTerrain
    {
        Instance = this;
        if (generateNewTrees)
        {
            trees = new GameObject[totalTrees];
            for (int i = 0; i < totalTrees; i++)
            {
                int randIndex = Random.Range(0, treePrefabs.Length);
                Quaternion treePrefabAngle = treePrefabs[randIndex].transform.rotation;
                trees[i] = Instantiate(treePrefabs[randIndex], Vector3.zero, Quaternion.Euler(treePrefabAngle.eulerAngles.x, treePrefabAngle.eulerAngles.y, treePrefabAngle.eulerAngles.z));
                trees[i].transform.parent = transform;
                foreach (Renderer r in trees[i].GetComponentsInChildren<Renderer>())
                {
                    r.enabled = false;
                }
            }
        }        
    }

    // Use this for initialization
    /*void Start () {
        trees = new GameObject[totalTrees];
        for(int i = 0; i < totalTrees; i++)
        {
            trees[i] = Instantiate(treePrefabs[Random.Range(0, treePrefabs.Length)], Vector3.zero, Quaternion.identity);
            foreach (Renderer r in trees[i].GetComponentsInChildren<Renderer>())
            {
                r.enabled = false;
            }
        }
	}*/
	
    /// <summary>
    /// Smart planes can call this function to receive a tree.
    /// </summary>
    /// <returns>Returns a tree object. Returns null if there are no available trees in the treePool.</returns>
	static public GameObject getTree()
    {
        for (int i = 0; i < totalTrees; i++)
        {
            foreach (Renderer r in trees[i].GetComponentsInChildren<Renderer>())
            {
                if (r.enabled)
                    goto iteration_end;
            }
            return trees[i];

            iteration_end:;                        
        }
        return null;
    }
}
