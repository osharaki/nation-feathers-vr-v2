﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VR;

/// <summary>
/// Controls player movement and various other functions.
/// </summary>
public class PlayerController : MonoBehaviour {

    //holistic3d tutorial
    Vector2 mouseLook;
    Vector2 smoothV;
    public float sensitivity = 5f;
    public float smoothing = 2f;

    public float mouseSensitivity;
    public float clampAngle = 80f;
    public float VRSpeed, editorSpeed;
    public bool isGrounded = false; /*!< Determines whether player can move.*/
    public float preferredHeight = 2f;
    public bool useLerpedMouse, useSmoothingMouse, useNormalMouse;
    public float smoothTime;
    public Transform cameraContainer;
    public LayerMask myLayerMask;
    public GameObject MovePrompt;
    public float addedRotation;

    private Text text;
    private float rotX = 0f, rotY = 0f;
    private Transform cameraTransform;
    //private Rigidbody rb;
    private float smoothMoveX;
    private float smoothMoveY;
    private Vector2 dir;
    private float speed;
    private bool initialTouch;

    void Start()
    {
        initialTouch = true;
        speed = VRSpeed;
        text = GetComponentInChildren<Text>();
        useSmoothingMouse = true;
        //rb = GetComponent<Rigidbody>();
        cameraTransform = Camera.main.transform;
        Vector3 rot = transform.localRotation.eulerAngles;
        rotX = rot.x;
        rotY = rot.y;
        if (Application.platform == RuntimePlatform.WindowsEditor) //Turn on keyboard and mouse controls if running on PC
        {
            cameraContainer.parent = transform;
            cameraContainer.transform.localRotation = Quaternion.Euler(0, 0, 0);
            speed = editorSpeed;
        }
    }

    void Update()
    {
        OVRInput.Update();
        if (Application.platform == RuntimePlatform.WindowsEditor) //Turn on keyboard and mouse controls if running on PC
        {
            //Debug.Log("Editor");
            if (useLerpedMouse)
            {
                LookAtMouseLerp();
            }
            if (useNormalMouse)
            {
                LookAtMouse();
            }
            if (useSmoothingMouse)
            {
                LookAtMouseSmoothing();
            }
            //float movementHorizontal = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
            //float movementVertical = Input.GetAxis("Vertical") * speed * Time.deltaTime;
            dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * speed * Time.deltaTime;
            if(GameController.instance != null)
            {
                if (GameController.instance.sceneEnum == GameController.SceneName.INTRO)
                {
                    if (dir != Vector2.zero)
                    {
                        if (!initialTouch)
                        {
                            initialTouch = true; //So we don't execute this section again
                                                 //Find animator in MovePrompt
                            if (MovePrompt != null)
                            {
                                Animator movePromptAnimator = MovePrompt.GetComponent<Animator>();
                                if (movePromptAnimator != null)
                                    IntroInterfaceController.instance.PlayAnimation(movePromptAnimator, "FadeOut");
                            }
                        }
                    }
                }
            }                       
        }   
        else if (Application.platform == RuntimePlatform.Android)
        {
            //handle Cotroller touch for movement
            if(OVRManager.isHmdPresent)
            {
                //cameraTransform.rotation = InputTracking.GetLocalRotation(VRNode.Head);
                //cameraTransform.localPosition = InputTracking.GetLocalPosition(VRNode.Head);
                transform.rotation = Quaternion.Euler(0, cameraTransform.rotation.eulerAngles.y, 0);
                
                dir = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad) * speed;
                if (GameController.instance.sceneEnum == GameController.SceneName.INTRO)
                {
                    if (dir != Vector2.zero) //If player has moved
                    {
                        if (!initialTouch)
                        {
                            initialTouch = true; //So we don't execute this section again
                                                 //Find animator in MovePrompt
                            if(MovePrompt != null)
                            {
                                Animator movePromptAnimator = MovePrompt.GetComponent<Animator>();
                                if (movePromptAnimator != null)
                                    IntroInterfaceController.instance.PlayAnimation(movePromptAnimator, "FadeOut");
                            }                            
                        }
                    }
                }
                                
                if (text != null)
                {
                    text.text = transform.rotation.ToString();
                }                            
            }
            else
            {
                //handle normal mobile input
            }            
        }
        if(!isGrounded)
            Move(); //Apply input to movement
    }

    /// <summary>
    /// Turns camera and capsule to face pointer. Camera can rotate both on y and x axes (up/down & left/right). 
    /// Capsule rotates only on y-axis (left & right)
    /// Based on: http://answers.unity3d.com/questions/29741/mouse-look-script.html
    /// </summary>
    void LookAtMouse()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        rotX += -mouseY * mouseSensitivity;
        rotY += mouseX * mouseSensitivity;

        //Debug.Log("w/o " + (mouseX * Time.deltaTime) + ", w/ " + (mouseX * mouseSensitivity * Time.deltaTime) + ", mouseSensitivity: " + mouseSensitivity);

        rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

        transform.rotation = Quaternion.Euler(0f, rotY, 0f);
        cameraTransform.localRotation = Quaternion.Euler(rotX, 0f, 0f);
    }

    /// <summary>
    /// Turns camera and capsule to face pointer. Camera can rotate both on y and x axes (up/down & left/right). 
    /// Capsule rotates only on y-axis (left & right). Uses Lerp for motion smoothness.
    /// Source: https://www.youtube.com/watch?v=blO039OzUZc
    /// </summary>
    void LookAtMouseLerp()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
        smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
        smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
        mouseLook += smoothV;
        mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f);
        cameraTransform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        transform.localRotation = Quaternion.AngleAxis(mouseLook.x, transform.up);
    }

    /// <summary>
    /// Turns camera and capsule to face pointer. Camera can rotate both on y and x axes (up/down & left/right). 
    /// Capsule rotates only on y-axis (left & right). Uses SmoothDamp for extra motion smoothness and gradual stopping.  
    /// </summary>
    void LookAtMouseSmoothing()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
        smoothV.x = Mathf.SmoothDamp(smoothV.x, md.x, ref smoothMoveX, smoothTime);
        smoothV.y = Mathf.SmoothDamp(smoothV.y, md.y, ref smoothMoveY, smoothTime);
        mouseLook += smoothV;
        mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f);
        cameraTransform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        //Debug.Log(mouseLook.x);
        transform.localRotation = Quaternion.AngleAxis(mouseLook.x + (addedRotation), transform.up);
    }

    /// <summary>
    /// Uses transform.Translate to move the player. Uses raycasting to set the player's height according to
    /// the terrain beneath him.
    /// </summary>
    void Move()
    {
        //float movementHorizontal = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        //float movementVertical = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        float yTranslation = 0f;        
        
        //raycasting
        Ray ray = new Ray(transform.position, -transform.up);
        RaycastHit hitInfo;
        //Debug.DrawRay(ray.origin, (ray.direction * 100), Color.red);
        Physics.Raycast(ray, out hitInfo, myLayerMask);
        
        if (hitInfo.distance > preferredHeight)
        {
            //Debug.Log("distance is longer than preferred");
            yTranslation = (hitInfo.distance - preferredHeight) * -1;
        }
        else if (hitInfo.distance < preferredHeight)
        {
            //Debug.Log("distance is smaller than preferred");
            yTranslation = preferredHeight - hitInfo.distance;
        }
        //Debug.Log(yTranslation);
        transform.Translate(dir.x, yTranslation, dir.y);
        cameraContainer.position = transform.position;
    }
}
