﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CirclingBird : MonoBehaviour {

    public float speed,
                 timeCounter,
                 width,
                 height;
    public GameObject pivot;

	// Update is called once per frame
	void Update () {
        timeCounter += Time.deltaTime * speed;

        float x = Mathf.Cos(timeCounter) * width;
        float y = transform.position.y;
        float z = Mathf.Sin(timeCounter) * height;

        transform.position = new Vector3(x + pivot.transform.position.x, 
            y, 
            z + pivot.transform.position.z) ;
    }
}
