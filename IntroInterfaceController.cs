﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using VRStandardAssets.Utils;

/// <summary>
/// Intro scene's interface controller.
/// </summary>
public class IntroInterfaceController : MonoBehaviour {

    public static IntroInterfaceController instance;
    public GameObject circlingBird, 
                      dustContainer, 
                      infoboxContainer, 
                      player, 
                      lookUPPrompt,
                      lookRightPrompt,
                      GUIReticle,
                      clickPrompt,
                      unclickPrompt,
                      touchpadReleasePrompt,
                      HearPrompt,
                      gatewayArrow;
    //public Animation fadeOut;
    public Camera myCamera;
    public GameObject gazeHint; /*!< Contains canvas for displaying a message when player is ready to try gazing at an object.*/
    public bool gazed; /*!< Whether the player has already looked at the info box.*/

    private float roomToMove = 2f;
    //public float infoboxAltitude; /*!< Height of infobox relative to tree.*/

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        gazed = false;
        myCamera.GetComponentInChildren<VRInput>().OnTouchpadHold += ActivateTouchpadReleasePromptManager;
        //myCamera.GetComponentInChildren<VRInput>().OnClick += ActivateTouchpadReleasePromptManager;
    }

    void Update()
    {
        if(GameController.instance.checkDistance)
            CheckDistance();
    }

    /// <summary>
    /// Finds dust effect in tree where bird has landed.  
    /// </summary>
    /// <returns>Dust effect game object found.</returns>
    GameObject GetDust()
    {
        GameObject landingTree = circlingBird.GetComponent<MovingBird>().landingTree;
        foreach(Transform child in landingTree.transform)
        {
            if(child.gameObject.tag == "Dust Container")
            {
                return child.gameObject;
            }
        }
        return null;
    }

    /// <summary>
    /// Manages how infoboxes behave when their parent objects are clicked. 
    /// Called by HandleClick in VRInteractiveItemApplied. HandleClick is subscribed
    /// to OnClick.
    /// </summary>
    public void ManageInfobox()
    {
        Text aTextbox;
        Image anImage;
        GameObject videoQuad;
        aTextbox = infoboxContainer.GetComponentInChildren<Text>(true);
        anImage = infoboxContainer.GetComponentInChildren<Image>(true);
        videoQuad = infoboxContainer.GetComponentInChildren<VideoPlayer>(true).gameObject.transform.parent.gameObject;
        if (IntroAudioController.instance.isFocused && infoboxContainer != null
                                                && !infoboxContainer.activeSelf)
        {
            infoboxContainer.SetActive(true);
            if (anImage != null)
            {
                anImage.gameObject.SetActive(true);
            }
            //This is the first activation. 'Look up' prompt should be called here
            ActivateLookUpPrompt();
        }
        else if (IntroAudioController.instance.isFocused && infoboxContainer != null)
        {
            if (aTextbox != null)
            {
                aTextbox.gameObject.SetActive(true);
            }
            if(videoQuad != null)
            {
                videoQuad.SetActive(true);
            }
            else
            {
                Debug.Log("VideoQuad not found!");
            }
            if (anImage != null && !anImage.gameObject.activeSelf)
            {
                anImage.gameObject.SetActive(true);
            }                      
        }
        else if (!IntroAudioController.instance.isFocused)
        {
            if (aTextbox != null)
            {
                aTextbox.gameObject.SetActive(false);
            }
            if (videoQuad != null)
            {
                videoQuad.SetActive(false);
            }
        }
    }    

    public void ActivateDust(GameObject dustContainer)
    {
        if(dustContainer != null)
            dustContainer.transform.GetChild(0).gameObject.SetActive(true); //Activate dust effect.
    }

    public void DeactivateDust(GameObject dustContainer)
    {
        if(dustContainer != null)
            dustContainer.transform.GetChild(0).gameObject.SetActive(false); //Deactivate dust effect.
    }

    /// <summary>
    /// Checks to see if player is far enough from a focusedObject.
    /// If player is a certain distance away from focusedObject then the
    /// focusedObject is unfocused. More specifically, an unfocusing click is simulated.
    /// </summary>
    void CheckDistance()
    {
        if (IntroAudioController.instance.isFocused)
        {
            if (IntroAudioController.instance.focusedObject != null)
            {
                /*if (Vector3.Distance(player.transform.position,
                    IntroAudioController.instance.focusedObject.transform.position) >
                                    myCamera.GetComponent<VREyeRaycaster>().m_RayLength + 15)
                {
                    //Simulate an unfocusing click
                    IntroAudioController.instance.focusedObject.GetComponent<IntroVRInteractiveItemApplied>().HandleClick();
                }*/
                if ((Vector2.Distance(new Vector2(player.transform.position.x, player.transform.position.z)
                    , new Vector2(IntroAudioController.instance.focusedObject.transform.position.x,
                                  IntroAudioController.instance.focusedObject.transform.position.z)) -
                                  circlingBird.GetComponent<MovingBird>().treesInVicinity[0].GetComponent<CapsuleCollider>().radius)
                                         > myCamera.GetComponent<VREyeRaycaster>().m_RayLength + roomToMove)
                {
                    IntroAudioController.instance.focusedObject.GetComponent<IntroVRInteractiveItemApplied>().HandleClick();
                }
            }
        }
    }

    public void EnableGazeHint()
    {
        gazeHint.SetActive(true);
    }

    public void DisableGazeHint()
    {
        gazeHint.SetActive(false);
    }

    public void ManageGazeHint()
    {
        if (gazed)
        {
            GameController.instance.narrative = GameController.NarrativeState.HIGHLIGHTED;
        }
    }

    /// <summary>
    /// Called on first activation of an infobox. 
    /// Displays a prompt to the player to look up if infobox
    /// is out of camera view when player clicked a tree.
    /// Disables reticle while it's active to prevent focus distortion.
    /// </summary>
    void ActivateLookUpPrompt()
    {
        //Image UpIcon = null;
        //Text UpText = null;
        Image image = infoboxContainer.GetComponentInChildren<Image>(true);
        Vector3 check = myCamera.WorldToViewportPoint(image.transform.position);
        //Debug.Log(check);
        if (!((check.x >= 0 && check.x <= 1) && 
            (check.y >= 0 && check.y <= 1) &&
            (check.z >= 0)))
        {
            //Debug.Log("Target not in camera view. Add look up.");
            //Target not in camera view. Add look up.
            //Look for UpIcon in hierarchy.
            
            lookUPPrompt.gameObject.SetActive(true);
            GUIReticle.SetActive(false);
            StartCoroutine("DeactivateLookUpPrompt");
        }        
    }

    /// <summary>
    /// Deactivates the prompt created by ActivateLookUpPrompt
    /// when infobox appears in camera view as player moves his head.
    /// Reenables reticle.
    /// </summary>
    IEnumerator DeactivateLookUpPrompt()
    {
        //Image UpIcon = null;
        //Text UpText = null;
        Image image = infoboxContainer.GetComponentInChildren<Image>(true);
        Vector3 check = myCamera.WorldToViewportPoint(image.transform.position);
        //Debug.Log(check);
        while (!((check.x >= 0 && check.x <= 1) &&
            (check.y >= 0 && check.y <= 1) &&
            (check.z >= 0)))
        {
            //Debug.Log("Target still not in camera view.");
            check = myCamera.WorldToViewportPoint(image.transform.position);
            //Target not in camera view. Add look up.
            yield return null;
        }
        //Debug.Log("Target in camera view.");
        //Look for UpIcon in hierarchy.
        /*foreach (Transform t in lookUPPrompt.transform)
        {
            if (t.gameObject.tag == "LookUpPrompt")
            {
                //UpIcon = t.GetComponentInChildren<Image>();
                //UpText = t.GetComponentInChildren<Text>();
                t.gameObject.SetActive(false);
                GUIReticle.SetActive(true);
            }
        }*/
        lookUPPrompt.gameObject.SetActive(false);
        GUIReticle.SetActive(true);
        /*if (UpIcon != null)        
            UpIcon.gameObject.SetActive(false);
        if (UpText != null)
            UpText.gameObject.SetActive(true);*/
    }

    /// <summary>
    /// Called from GameController when NarrativeState is HIGHLIGHTED.
    /// </summary>
    public void ActivateClickPrompt()
    {
        clickPrompt.SetActive(true);
        StartCoroutine("DeactivateClickPrompt");
    }

    /// <summary>
    /// Called by ActivateClickPrompt
    /// </summary>
    IEnumerator DeactivateClickPrompt()
    {
        while (!IntroAudioController.instance.isFocused) //wait till the tree becomes focused.
        {
            yield return null;
        }
        clickPrompt.SetActive(false);
        //Move on to next state
        GameController.instance.narrative = GameController.NarrativeState.FOCUSED; 
    }

    /// <summary>
    /// Called from GameController. Activates unclickPrompt and starts
    /// coroutine DeactivateUnclickPrompt to disable unclickPrompt when 
    /// player has clicked trigger to deactivate object.
    /// </summary>
    public void ActivateUnclickPrompt()
    {
        unclickPrompt.SetActive(true);
        StartCoroutine("DeactivateUnclickPrompt");
    }

    /// <summary>
    /// Called by ActivateUnclickPrompt. Deactivates unclickPrompt when
    /// player clicks trigger to deactivate object. Sets GameController's
    /// narrative field to move on to next NarrativeState.
    /// </summary>
    /// <returns></returns>
    IEnumerator DeactivateUnclickPrompt()
    {
        while (IntroAudioController.instance.isFocused)//Wait while object still focused.
        {
            yield return null;
        }
        unclickPrompt.SetActive(false);
        GameController.instance.narrative = GameController.NarrativeState.UNFOCUSED;
    }

    void ActivateTouchpadReleasePromptManager()
    {
        StartCoroutine("TouchpadReleasePromptManager");
    }

    /// <summary>
    /// Manages prompt to notify player to release touchpad button when it
    /// is held down for a certain amount of time. 
    /// Also activates fade out animation before finally deactivating the prompt.
    /// Is subscribed to VRInput's OnTouchpadHold event.
    /// Disables camera reticle while active.
    /// </summary>
    /// <returns></returns>
    IEnumerator TouchpadReleasePromptManager()
    {
        GUIReticle.SetActive(false);
        if (!touchpadReleasePrompt.activeSelf)
        {
            touchpadReleasePrompt.SetActive(true);
            //yield return new WaitForSeconds(2);
            //fadeOut.Play();
            //check if animation is playing
            while (touchpadReleasePrompt.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("FadeOut"))
            {
                //animation is playing, wait..
                //Debug.Log("Still waiting");
                yield return null;
            }
            //Debug.Log("Prompt disabled");
            touchpadReleasePrompt.SetActive(false);
            GUIReticle.SetActive(true);
        }        
    }

    public void ActivateLookRightPrompt(GameObject tree)
    {
        lookRightPrompt.SetActive(true);
        RawImage rawImage = tree.GetComponentInChildren<RawImage>();
        if(rawImage != null)
        {
            lookRightPrompt.transform.parent = rawImage.transform.parent;
            lookRightPrompt.transform.localPosition =
                                new Vector3(-1.27f, -0.5f, 0);
            lookRightPrompt.transform.rotation = Quaternion.Euler(0, 0, -90);
            lookRightPrompt.transform.localScale = new Vector3(0.03f, 0.03f, 1);
        }        
    }

    public void DeactivateLookRightPrompt()
    {
        lookRightPrompt.SetActive(false);
    }    

    public void PlayAnimation(Animator animator, string animName)
    {
        animator.Play(animName, 0);
    }

    /// <summary>
    /// Activates HearPrompt. Starts Coroutine DeactivateHearPrompt.
    /// </summary>
    public void ActivateHearPrompt()
    {
        HearPrompt.SetActive(true);
        StartCoroutine("DeactivateHearPrompt");
    }

    /// <summary>
    /// Checks status of alpha in text color. Disables HearPrompt
    /// when alpha reaches 0. Started by ActivateHearPrompt.
    /// </summary>
    /// <returns></returns>
    IEnumerator DeactivateHearPrompt()
    {
        //yield return new WaitForSeconds(1); //Give alpha a chance to increase from 0
        while(HearPrompt.GetComponentInChildren<Text>().color.a != 0)
        {
            yield return null;
        }
        HearPrompt.SetActive(false);
    }

    public void ActivateGatewayArrow()
    {
        gatewayArrow.SetActive(true);
    }
}
