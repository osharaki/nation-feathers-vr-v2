﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

/// <summary>
/// The gameobject this is attached to will be placed somewhere within the camera's frustum.
/// </summary>
public class PlaceInSight : MonoBehaviour {

    public Camera myCamera;
    public float distFromCamera;
    public bool useNormal;                

    private Vector3 originalScale;
    private Quaternion originalRotation;
    //private Vector3 endOfRay;

    void Start () {        
        originalScale = transform.localScale;
        originalRotation = transform.localRotation;

        VREyeRaycaster raycaster = myCamera.GetComponent<VREyeRaycaster>();
        raycaster.OnRaycasthit += SetUIPosition;
        raycaster.OnRaycastNohit += SetUIPosition;
    }

    public void SetUIPosition(RaycastHit hit)
    {
        transform.position = hit.point;
        transform.localScale = originalScale * hit.distance;

        // If the reticle should use the normal of what has been hit...
        if (useNormal)
            // ... set it's rotation based on it's forward vector facing along the normal.
            transform.rotation = Quaternion.FromToRotation(Vector3.forward, hit.normal);
        else
            // However if it isn't using the normal then it's local rotation should be as it was originally.
            transform.localRotation = originalRotation;
    }

    public void SetUIPosition()
    {
        // Set the position of the reticle to the default distance in front of the camera.
        transform.position = myCamera.transform.position 
                             + myCamera.transform.forward 
                             * distFromCamera;

        // Set the scale based on the original and the distance from the camera.
        transform.localScale = originalScale * distFromCamera;

        // The rotation should just be the default.
        transform.localRotation = originalRotation;
    }
}
