﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraRenderChecker : MonoBehaviour {

    public Text text;
    public Image image;
    public GameObject reticle;

    private float textRatio, /*!< Scale to Z position ratio of text.*/
                  imageRatio;/*!< Scale to Z position ratio of image.*/
    private Vector3 textOrigScale, imageOrigScale;

    void Start()
    {
        textRatio = text.transform.position.z / text.transform.localScale.x;
        imageRatio = image.transform.position.z / image.transform.localScale.x;
        textOrigScale = text.transform.localScale;
        imageOrigScale = image.transform.localScale;
    }

    void Update()
    {
        text.transform.localPosition = new Vector3(text.transform.localPosition.x,
                                             text.transform.localPosition.y,
                                             reticle.transform.localPosition.z);
        image.transform.localPosition = new Vector3(image.transform.localPosition.x,
                                               image.transform.localPosition.y,
                                               reticle.transform.localPosition.z);
        /*text.transform.localScale = new Vector3(text.transform.position.z / textRatio,
                                                text.transform.position.z / textRatio,
                                                text.transform.localScale.z);
        image.transform.localScale = new Vector3(image.transform.position.z / imageRatio,
                                                 image.transform.position.z / imageRatio,
                                                 image.transform.localScale.z);*/
        text.transform.localScale = textOrigScale * text.transform.position.z;
        image.transform.localScale = imageOrigScale * text.transform.position.z;
    }
	/*void OnWillRenderObject()
    {
        Debug.Log("Object will be rendered!");
        //IntroInterfaceController.instance.DeactivateLookUpPrompt();
    }*/
}
