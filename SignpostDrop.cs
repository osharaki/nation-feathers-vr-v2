﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Drops signpost from a certain height and stops it when it hits the ground.
/// Attached to signpost objects.
/// </summary>
public class SignpostDrop : MonoBehaviour {

    public float startingHeight, /*!< The height from which the signpost will fall*/
                 fallRate; 

    private float origHeight; /*!< The initial and final height of the signpost.*/
    private AnswerManager answerManager;
	
    private void Start()
    {
        answerManager = transform.parent.gameObject.GetComponent<AnswerManager>();
    }
    // Use this for initialization
	/*void Start ()
    {
        origHeight = transform.position.y;
        gameObject.SetActive(true);
        transform.position = new Vector3(transform.position.x,
                                         startingHeight,
                                         transform.position.z);
        StartCoroutine("Fall");
    }*/

    /*void OnEnable()
    {
        //Signpost will rotate to face the player when it's first enabled.
        transform.rotation = Quaternion.LookRotation(transform.position -
            Camera.main.transform.position); //rotates the billboard
                                                //locks rotation on x-z-axes
        transform.rotation = Quaternion.Euler(0,
                                              transform.rotation.eulerAngles.y + 180,
                                              0);
    }*/

    /// <summary>
    /// Moves signpost forward a certain distance then drops it vertically 
    /// and stops it when it hits the ground. Sound is played on hit.
    /// </summary>
    /// <returns></returns>
    IEnumerator Fall()
    {
        origHeight = transform.position.y;

        transform.localPosition = new Vector3(0, 0, 0);

        //Signpost will rotate to face the player before it
        //starts falling.
        transform.rotation = Quaternion.LookRotation(transform.position -
            Camera.main.transform.position); //rotates the billboard
                                             //locks rotation on x-z-axes
        transform.rotation = Quaternion.Euler(0,
                                              transform.rotation.eulerAngles.y + 180,
                                              0);
        
        transform.position += transform.forward * 10;
        transform.position += transform.up * startingHeight;
        //StartCoroutine("Fall");
        while (transform.position.y > origHeight)
        {
            transform.position = new Vector3(transform.position.x,
                                             transform.position.y - fallRate,
                                             transform.position.z);
            yield return null;
        }
        transform.position = new Vector3(transform.position.x,
                                         origHeight,
                                         transform.position.z);
        AudioSource TouchGroundAS = GetComponent<AudioSource>();
        if(TouchGroundAS != null)
        {
            TouchGroundAS.Play();
        }
    }

    /// <summary>
    /// Removes signpost after a delay before enabling the infobox.
    /// Disables tree interactability during delay to prevent shifting 
    /// focus from tree while infobox is set to appear. This prevents
    /// a situation where the tree is no longer in focus, but its 
    /// whole infobox is visible.
    /// </summary>
    /// <param name="delay"></param>
    /// <returns></returns>
    public IEnumerator RemoveSignpost(float delay, SpriteRenderer sr)
    {
        ChallengeVRInteractiveItemApplied interactionScript =
            transform.parent.gameObject.GetComponent<ChallengeVRInteractiveItemApplied>();
        if (interactionScript != null)
            interactionScript.enabled = false;
        yield return new WaitForSeconds(delay);
        DeactivateSignpostComponents();
        if(answerManager != null)
            answerManager.ActivateInfobox(sr);
        if (interactionScript != null)
            interactionScript.enabled = true;
    }
    public void DeactivateSignpostComponents()
    {
        foreach (Transform signpostChild in transform)
        {
            signpostChild.gameObject.SetActive(false);
        }
    }

    public void ActivateSignpostComponents()
    {

        foreach (Transform signpostChild in transform)
        {
            signpostChild.gameObject.SetActive(true);
        }
    }

    public void FallStarter()
    {
        StartCoroutine("Fall");
    }

    public void RemoveStarter(float delay, SpriteRenderer sr)
    {        
        StartCoroutine(RemoveSignpost(delay, sr));
    }
}
